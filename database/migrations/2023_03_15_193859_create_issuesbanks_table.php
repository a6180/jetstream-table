<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('issuesbanks', function (Blueprint $table) {
            $table->id();
            $table->string('module', 100)->nullable();
            $table->string('detail', 100)->nullable();
            $table->string('root_cause', 100)->nullable();
            $table->string('solutions', 100)->nullable();
            $table->string('changefile', 100)->nullable();
            $table->string('changedb', 100)->nullable();
            $table->string('changehw', 100)->nullable();
            $table->string('status', 100)->nullable();
            $table->string('reportby', 100)->nullable();
            $table->string('pic', 100)->nullable();
            $table->string('remark', 100)->nullable();
            $table->date('root_date')->nullable();
            $table->date('fix_date')->nullable();
            $table->string('branch_code',2)->nullable();
            $table->string('current_team_id',2)->nullable();
            $table->string('active',2)->nullable();
            $table->softDeletes()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('issuesbanks');
    }
};
