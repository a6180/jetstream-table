<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->id();
            $table->string('name_usr', 100)->nullable();
            $table->string('email_usr', 100)->nullable();
            $table->string('pwd_usr', 100)->nullable();
            $table->string('email_type', 100)->nullable();
            $table->string('dept_usr', 100)->nullable();
            $table->string('remark_usr', 100)->nullable();
            $table->date('month_date')->nullable();
            $table->string('branch_code',2)->nullable();
            $table->string('current_team_id',2)->nullable();
            $table->string('active',2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('emails');
    }
};
