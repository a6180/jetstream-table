<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->date('renewal_date')->nullable();
            $table->string('namedisplay', 100)->nullable();
            $table->string('group', 100)->nullable();
            $table->string('dept', 100)->nullable();
            $table->string('lantai', 100)->nullable();
            $table->string('extnumber', 100)->nullable();
            $table->string('digital', 100)->nullable();
            $table->string('mail', 100)->nullable();
            $table->string('remote', 100)->nullable();
            $table->string('branch_code',2)->nullable();
            $table->string('current_team_id',2)->nullable();
            $table->string('active',2)->nullable();
            $table->softDeletes()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contacts');
    }
};
