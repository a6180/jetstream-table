<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('passwordmgrs', function (Blueprint $table) {
            $table->id();
            $table->string('address', 150)->nullable();
            $table->string('user', 100)->nullable();
            $table->string('pass', 100)->nullable();
            $table->string('type', 150)->nullable();
            $table->string('remark', 100)->nullable();
            $table->string('branch_code',2)->nullable();
            $table->string('current_team_id',2)->nullable();
            $table->string('active',2)->nullable();
            $table->string('status',15)->nullable();
            $table->softDeletes()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('passwords');
    }
};
