<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('assetkitas', function (Blueprint $table) {
            $table->id();
            $table->string('ip_comp', 100);
            $table->string('hostname_comp', 100);
            $table->string('userpc',50)->default(0)->nullable();
            $table->string('os_comp', 100)->nullable();
            $table->string('type_comp', 100)->nullable();
            $table->string('processor_comp', 100)->nullable();
            $table->string('ram_comp', 100)->nullable();
            $table->string('hdd_comp', 100)->nullable();
            $table->string('brand',80)->nullable();
            $table->string('dept_comp', 100)->nullable();
            $table->string('branch_code')->nullable();
            $table->string('flag')->nullable();
            $table->string('photo')->nullable();
            $table->string('file_name')->nullable();
            $table->string('remote')->nullable();
            $table->string('remark',50)->nullable();
            $table->string('current_team_id',2)->nullable();
            $table->string('active',2)->nullable();
            $table->timestamp('buy_at')->nullable();
            $table->timestamp('delete_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('assetkitas');
    }
};
