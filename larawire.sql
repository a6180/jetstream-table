/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100424 (10.4.24-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : larawire

 Target Server Type    : MySQL
 Target Server Version : 100424 (10.4.24-MariaDB)
 File Encoding         : 65001

 Date: 18/03/2023 02:29:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for assetkitas
-- ----------------------------
DROP TABLE IF EXISTS `assetkitas`;
CREATE TABLE `assetkitas`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_comp` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `hostname_comp` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `userpc` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0',
  `os_comp` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `type_comp` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `processor_comp` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ram_comp` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `hdd_comp` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `brand` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `dept_comp` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `branch_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `flag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remote` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `current_team_id` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `active` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `buy_at` timestamp NULL DEFAULT NULL,
  `delete_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assetkitas
-- ----------------------------
INSERT INTO `assetkitas` VALUES (10, '192.168.95.42', 'MGS 1', 'Haris', 'WIN xp', 'AiO', 'i5', '23', '200', 'DELL', 'Finishing', 'A1', '2', '1', '1.jpg', '234', '2', '1', '1', '2023-03-18 00:38:30', NULL, '2023-03-18 00:37:37', '2023-03-18 00:38:30');

-- ----------------------------
-- Table structure for authentication_log
-- ----------------------------
DROP TABLE IF EXISTS `authentication_log`;
CREATE TABLE `authentication_log`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `authenticatable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `authenticatable_id` bigint UNSIGNED NOT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `login_at` timestamp NULL DEFAULT NULL,
  `login_successful` tinyint(1) NOT NULL DEFAULT 0,
  `logout_at` timestamp NULL DEFAULT NULL,
  `cleared_by_user` tinyint(1) NOT NULL DEFAULT 0,
  `location` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `authentication_log_authenticatable_type_authenticatable_id_index`(`authenticatable_type` ASC, `authenticatable_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of authentication_log
-- ----------------------------
INSERT INTO `authentication_log` VALUES (1, 'App\\Models\\User', 1, '127.0.0.1s', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.69', NULL, 0, '2023-03-16 00:29:03', 0, NULL);
INSERT INTO `authentication_log` VALUES (2, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.69', NULL, 0, '2023-03-16 00:36:30', 0, NULL);
INSERT INTO `authentication_log` VALUES (3, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.69', '2023-03-16 00:36:40', 1, '2023-03-16 00:39:09', 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"US\",\"country\":\"United States\",\"city\":\"New Haven\",\"state\":\"CT\",\"state_name\":\"Connecticut\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"America\\/New_York\",\"continent\":\"NA\",\"currency\":\"USD\",\"default\":true,\"cached\":false}');
INSERT INTO `authentication_log` VALUES (4, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.69', '2023-03-16 00:39:18', 1, '2023-03-16 00:39:48', 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"US\",\"country\":\"United States\",\"city\":\"New Haven\",\"state\":\"CT\",\"state_name\":\"Connecticut\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"America\\/New_York\",\"continent\":\"NA\",\"currency\":\"USD\",\"default\":true,\"cached\":false}');
INSERT INTO `authentication_log` VALUES (5, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.69', '2023-03-16 00:39:57', 1, '2023-03-16 00:44:40', 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"US\",\"country\":\"United States\",\"city\":\"New Haven\",\"state\":\"CT\",\"state_name\":\"Connecticut\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"America\\/New_York\",\"continent\":\"NA\",\"currency\":\"USD\",\"default\":true,\"cached\":false}');
INSERT INTO `authentication_log` VALUES (6, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.69', '2023-03-16 00:45:11', 1, '2023-03-16 00:45:25', 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"US\",\"country\":\"United States\",\"city\":\"New Haven\",\"state\":\"CT\",\"state_name\":\"Connecticut\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"America\\/New_York\",\"continent\":\"NA\",\"currency\":\"USD\",\"default\":true,\"cached\":false}');
INSERT INTO `authentication_log` VALUES (7, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.69', '2023-03-16 00:53:51', 1, '2023-03-16 00:54:13', 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"US\",\"country\":\"United States\",\"city\":\"New Haven\",\"state\":\"CT\",\"state_name\":\"Connecticut\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"America\\/New_York\",\"continent\":\"NA\",\"currency\":\"USD\",\"default\":true,\"cached\":false}');
INSERT INTO `authentication_log` VALUES (8, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.69', '2023-03-16 00:54:17', 1, '2023-03-16 00:55:52', 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"ID\",\"country\":\"Jakarta\",\"city\":\"Tebet\",\"state\":\"IDK\",\"state_name\":\"Jaksel\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"Asia\\/Jakarta\",\"continent\":\"NA\",\"currency\":\"IDR\",\"default\":true,\"cached\":false}');
INSERT INTO `authentication_log` VALUES (9, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.69', '2023-03-16 00:56:12', 1, '2023-03-16 01:00:35', 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"ID\",\"country\":\"Yogyakarta\",\"city\":\"Bantul\",\"state\":\"IDK\",\"state_name\":\"Bantul\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"Asia\\/Jakarta\",\"continent\":\"NA\",\"currency\":\"IDR\",\"default\":true,\"cached\":false}');
INSERT INTO `authentication_log` VALUES (10, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.69', '2023-03-16 01:00:39', 1, '2023-03-16 01:03:12', 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"ID\",\"country\":\"Yogyakarta\",\"city\":\"Bantul\",\"state\":\"IDK\",\"state_name\":\"Bantul\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"Asia\\/Jakarta\",\"continent\":\"NA\",\"currency\":\"IDR\",\"default\":true,\"cached\":false}');
INSERT INTO `authentication_log` VALUES (11, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.69', '2023-03-16 01:03:19', 1, '2023-03-16 01:06:57', 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"ID\",\"country\":\"Yogyakarta\",\"city\":\"Bantul\",\"state\":\"IDK\",\"state_name\":\"Bantul\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"Asia\\/Jakarta\",\"continent\":\"NA\",\"currency\":\"IDR\",\"default\":true,\"cached\":false}');
INSERT INTO `authentication_log` VALUES (12, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.69', '2023-03-16 01:07:02', 1, NULL, 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"ID\",\"country\":\"Yogyakarta\",\"city\":\"Bantul\",\"state\":\"IDK\",\"state_name\":\"Bantul\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"Asia\\/Jakarta\",\"continent\":\"NA\",\"currency\":\"IDR\",\"default\":true,\"cached\":false}');
INSERT INTO `authentication_log` VALUES (13, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.41', '2023-03-16 11:34:16', 1, NULL, 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"ID\",\"country\":\"Yogyakarta\",\"city\":\"Bantul\",\"state\":\"IDK\",\"state_name\":\"Bantul\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"Asia\\/Jakarta\",\"continent\":\"NA\",\"currency\":\"IDR\",\"default\":true,\"cached\":false}');
INSERT INTO `authentication_log` VALUES (14, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.41', '2023-03-17 00:39:41', 1, NULL, 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"ID\",\"country\":\"Yogyakarta\",\"city\":\"Bantul\",\"state\":\"IDK\",\"state_name\":\"Bantul\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"Asia\\/Jakarta\",\"continent\":\"NA\",\"currency\":\"IDR\",\"default\":true,\"cached\":false}');
INSERT INTO `authentication_log` VALUES (15, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.41', '2023-03-17 21:52:34', 1, '2023-03-18 02:23:57', 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"ID\",\"country\":\"Yogyakarta\",\"city\":\"Bantul\",\"state\":\"IDK\",\"state_name\":\"Bantul\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"Asia\\/Jakarta\",\"continent\":\"NA\",\"currency\":\"IDR\",\"default\":true,\"cached\":false}');
INSERT INTO `authentication_log` VALUES (16, 'App\\Models\\User', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.41', '2023-03-18 02:24:09', 1, NULL, 0, '{\"ip\":\"127.0.0.0\",\"iso_code\":\"ID\",\"country\":\"Yogyakarta\",\"city\":\"Bantul\",\"state\":\"IDK\",\"state_name\":\"Bantul\",\"postal_code\":\"06510\",\"lat\":41.31,\"lon\":-72.92,\"timezone\":\"Asia\\/Jakarta\",\"continent\":\"NA\",\"currency\":\"IDR\",\"default\":true,\"cached\":false}');

-- ----------------------------
-- Table structure for branchoffices
-- ----------------------------
DROP TABLE IF EXISTS `branchoffices`;
CREATE TABLE `branchoffices`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `l1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `l2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `l3` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `build_date` date NOT NULL,
  `opening_date` date NOT NULL,
  `address1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address3` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_land` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `npwp` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_team_id` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of branchoffices
-- ----------------------------
INSERT INTO `branchoffices` VALUES (1, '1', '1', '1', '1', '1', '1', '2023-01-14', '2023-01-14', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, '2023-01-14 00:10:29', '2023-01-14 00:10:32');

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `renewal_date` date NULL DEFAULT NULL,
  `namedisplay` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `group` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `dept` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `lantai` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `extnumber` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `digital` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `mail` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remote` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `branch_code` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `current_team_id` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `active` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES (39, '2023-03-17', 'Haris Rifai', '1', 'HRD GA', '2', '212', 'Digital', 'ajusvc@yahoo.com', '234', 'A1', '1', '1', NULL, '2023-03-17 23:51:12', '2023-03-18 00:39:21');

-- ----------------------------
-- Table structure for departements
-- ----------------------------
DROP TABLE IF EXISTS `departements`;
CREATE TABLE `departements`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `group` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dept` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_team_id` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of departements
-- ----------------------------
INSERT INTO `departements` VALUES (1, 'BLI', 'Accounting', '1', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (2, 'BLI', 'Finishing', '1', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (3, 'BLI', 'Commercial', '1', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (4, 'BLI', 'HRD GA', '1', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (5, 'BLI', 'Design', '1', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (6, 'BLI', 'Production', '1', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (7, 'BLI', 'IT 2', '1', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (8, 'BLI', 'Purchase general', '1', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (9, 'BLI', 'Sample', '1', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (10, 'BLI', 'Cutting', '1', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (11, 'BLI', 'Warehouse', '1', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (12, 'BLI', 'IT 2', '3', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (13, 'BLI', 'MD', '3', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (14, 'DEMO', 'Commercial', '3', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');
INSERT INTO `departements` VALUES (15, 'BLI', 'DEPT. LA', '0', '1', '2023-01-14 00:09:04', '2023-01-14 00:09:07');
INSERT INTO `departements` VALUES (16, 'BLI', 'Accounting', '3', '1', '2022-04-01 13:17:19', '2022-04-01 13:17:19');

-- ----------------------------
-- Table structure for emails
-- ----------------------------
DROP TABLE IF EXISTS `emails`;
CREATE TABLE `emails`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_usr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_usr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `pwd_usr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `dept_usr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remark_usr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `month_date` date NULL DEFAULT NULL,
  `branch_code` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `current_team_id` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `active` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of emails
-- ----------------------------
INSERT INTO `emails` VALUES (1, 'Haris 2', 'haris@bli.co.id', 'welcome123', 'O365', 'Accounting', 'newa', '2023-03-17', 'A1', '1', '1', NULL, '2023-03-17 23:38:42');
INSERT INTO `emails` VALUES (2, 'Haris', 'haris@bli.co.id', 'welcome123', 'Other', 'HRD GA', 'new', '2023-03-17', 'A1', '1', '1', NULL, '2023-03-17 01:51:24');
INSERT INTO `emails` VALUES (10, 'Haris rifai', 'jiwa.yang.mati.rasa@gmail.com', '11122233', 'Hosting', 'Accounting', '3333', '2023-03-17', 'A1', '1', '0', '2023-03-17 01:51:06', '2023-03-17 01:51:12');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for issuesbanks
-- ----------------------------
DROP TABLE IF EXISTS `issuesbanks`;
CREATE TABLE `issuesbanks`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `module` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `detail` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `root_cause` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `solutions` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `changefile` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `changedb` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `changehw` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `reportby` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `pic` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `root_date` date NULL DEFAULT NULL,
  `fix_date` date NULL DEFAULT NULL,
  `branch_code` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `current_team_id` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `active` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of issuesbanks
-- ----------------------------
INSERT INTO `issuesbanks` VALUES (1, 'ATM', 'not payout', 'DB3', 'Fix DB & HW', 'config.xml', 'attribute', 'Fix lan card', 'Pending', 'Haris Rifai', 'haris', '1', '2023-03-18', '2023-03-18', 'A1', '1', '1', NULL, '2023-03-18 01:41:36', '2023-03-18 02:14:12');
INSERT INTO `issuesbanks` VALUES (2, 'CBS', 'not payout', 'DB4', 'Fix DB', 'config.xml', 'attribute', '0', '0', 'Haris Rifai', 'haris', '1', '2023-03-18', '2023-03-18', 'A1', '1', '1', NULL, '2023-03-18 01:41:36', '2023-03-18 01:53:31');
INSERT INTO `issuesbanks` VALUES (3, 'CBS', 'not payout', 'DB2', 'Fix DB', 'config.xml', 'attribute', '0', 'Pending', 'Haris Rifai', 'haris', '1', '2023-03-18', '2023-03-18', 'A1', '1', '1', NULL, '2023-03-18 01:41:36', '2023-03-18 02:09:27');
INSERT INTO `issuesbanks` VALUES (4, 'CBS', 'not payout', 'DB2', 'Fix DB', 'config.xml', 'attribute', '0', 'Solve', 'Haris Rifai', 'haris', '1', '2023-03-18', '2023-03-18', 'A1', '1', '1', NULL, '2023-03-18 01:41:36', '2023-03-18 02:07:46');
INSERT INTO `issuesbanks` VALUES (5, 'CBS', 'not payout', 'DB2', 'Fix DB', 'config.xml', 'attribute', '0', '0', 'Haris Rifai', 'haris', '1', '2023-03-18', '2023-03-18', 'A1', '1', '1', NULL, '2023-03-18 01:41:36', '2023-03-18 01:51:30');
INSERT INTO `issuesbanks` VALUES (6, 'CBS', 'not payout', 'DB', 'Fix DB', 'config.xml', 'attribute', '0', 'Solve', 'Haris Rifai', 'haris', '1', '2023-03-18', '2023-03-18', 'A1', '1', '1', NULL, '2023-03-18 01:41:36', '2023-03-18 02:20:16');
INSERT INTO `issuesbanks` VALUES (7, 'DB2', 'user compline annoying ', 'can\'t transaction CBS', 'status', 'status', 'status', 'status', 'Open', 'Haris Rifai', '', '', '2023-03-18', '2023-03-18', 'A1', '1', '1', NULL, '2023-03-18 01:46:23', '2023-03-18 02:20:34');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_reset_tokens_table', 1);
INSERT INTO `migrations` VALUES (3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (6, '2020_05_21_100000_create_teams_table', 1);
INSERT INTO `migrations` VALUES (7, '2020_05_21_200000_create_team_user_table', 1);
INSERT INTO `migrations` VALUES (8, '2020_05_21_300000_create_team_invitations_table', 1);
INSERT INTO `migrations` VALUES (9, '2021_10_24_153130_create_departements_table', 1);
INSERT INTO `migrations` VALUES (10, '2023_01_12_231139_create_settings_table', 1);
INSERT INTO `migrations` VALUES (11, '2023_01_13_153851_create_branchoffices_table', 1);
INSERT INTO `migrations` VALUES (12, '2023_03_11_070750_create_sessions_table', 1);
INSERT INTO `migrations` VALUES (13, '2023_03_11_071905_create_assetkitas_table', 1);
INSERT INTO `migrations` VALUES (14, '2023_03_11_091817_create_posts_table', 2);
INSERT INTO `migrations` VALUES (15, '2023_03_11_093651_add_detail_to_post_table', 3);
INSERT INTO `migrations` VALUES (17, '2023_03_11_135632_add_status_to_posts_table', 4);
INSERT INTO `migrations` VALUES (18, '2014_10_12_000000_create_users_table', 5);
INSERT INTO `migrations` VALUES (19, '2023_03_12_055229_add_userid_to_posts_table', 6);
INSERT INTO `migrations` VALUES (20, '2023_03_12_102340_add_date_to_posts_table', 7);
INSERT INTO `migrations` VALUES (21, '2023_03_14_224908_create_passwords_table', 8);
INSERT INTO `migrations` VALUES (22, '2023_03_15_153210_create_emails_table', 9);
INSERT INTO `migrations` VALUES (23, '2023_03_15_193859_create_issuesbanks_table', 9);
INSERT INTO `migrations` VALUES (24, '2023_03_16_001911_create_authentication_log_table', 9);
INSERT INTO `migrations` VALUES (25, '2023_03_16_014238_create_contacts_table', 10);

-- ----------------------------
-- Table structure for password_reset_tokens
-- ----------------------------
DROP TABLE IF EXISTS `password_reset_tokens`;
CREATE TABLE `password_reset_tokens`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_reset_tokens
-- ----------------------------
INSERT INTO `password_reset_tokens` VALUES ('moh.h.rifai@gmail.com', '$2y$10$Ui2cK5OKu7EhaIiYlzcDMOg/xGbF1k2XOKL3fRvBtKL02uBngl8jW', '2023-03-16 00:53:35');

-- ----------------------------
-- Table structure for passwordmgrs
-- ----------------------------
DROP TABLE IF EXISTS `passwordmgrs`;
CREATE TABLE `passwordmgrs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `address` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `pass` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `type` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `branch_code` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `current_team_id` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `active` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of passwordmgrs
-- ----------------------------
INSERT INTO `passwordmgrs` VALUES (1, '10.234.27.10', 'root', 'pwd123', 'root', '1', 'A1', '1', '0', '1', NULL, '2023-03-14 23:36:07', '2023-03-15 00:08:32');
INSERT INTO `passwordmgrs` VALUES (2, '10.234.27.11', 'root', 'pwd123', 'root', 'antivirus', 'A1', '1', '1', '1', NULL, '2023-03-14 23:36:07', '2023-03-15 00:43:24');
INSERT INTO `passwordmgrs` VALUES (3, '10.234.27.11', 'root', 'pwd123', 'root', '1', 'A1', '1', '1', '1', NULL, '2023-03-14 23:36:07', '2023-03-15 00:08:05');
INSERT INTO `passwordmgrs` VALUES (4, '10.234.27.11', 'root', 'pwd123', 'root', '1', 'A1', '1', '1', '1', NULL, '2023-03-14 23:36:07', '2023-03-15 00:08:05');
INSERT INTO `passwordmgrs` VALUES (5, '10.234.27.11', 'root', 'pwd123', 'root', '1', 'A1', '1', '1', '1', NULL, '2023-03-14 23:36:07', '2023-03-15 00:08:05');
INSERT INTO `passwordmgrs` VALUES (6, '10.234.27.11', 'root', 'pwd123', 'root', '1', 'A1', '1', '1', '1', NULL, '2023-03-14 23:36:07', '2023-03-15 00:08:05');
INSERT INTO `passwordmgrs` VALUES (7, '10.234.27.11', 'root', 'pwd123', 'root', '1', 'A1', '1', '1', '1', NULL, '2023-03-14 23:36:07', '2023-03-15 00:08:05');
INSERT INTO `passwordmgrs` VALUES (8, '10.234.27.11', '10.234.27.11', '10.234.27.11', '10.234.27.11', '10.234.27.11', 'A1', '1', '1', '1', NULL, '2023-03-15 00:42:33', '2023-03-15 00:42:33');

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token` ASC) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type` ASC, `tokenable_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dept_id` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `current_team_id` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `branch_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `active` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `remark` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_id` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `flag` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `date_at` timestamp NULL DEFAULT NULL,
  `date_close` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES (2, '        \'status\' => \'required\',', 'baru aja di edit', '1', '1', 'A1', '1', 1, NULL, '1', 'temp', NULL, '2023-03-14 00:13:58', '2023-02-28 09:24:31', '2023-03-14 00:13:58');
INSERT INTO `posts` VALUES (3, 'DBA', 'session()->flash(\'message\', \'Disable Successfully.\');', '1', '1', 'A1', '1', 1, NULL, '1', 'temp', NULL, '2023-03-14 02:50:41', '2023-03-09 09:24:31', '2023-03-14 02:50:41');
INSERT INTO `posts` VALUES (5, 'Haris', 'sdasdsadas', '1', '1', 'A1', '1', 0, NULL, '1', 'temp', NULL, NULL, '2023-03-01 09:24:31', '2023-03-11 13:28:52');
INSERT INTO `posts` VALUES (6, 'Pasang Modem Gsm', 'Penambahan Modem Backup1', '2', '1', 'A1', '1', 0, NULL, '1', 'temp', NULL, NULL, '2023-03-10 09:24:31', '2023-03-11 10:52:37');
INSERT INTO `posts` VALUES (7, 'Instalasi Network', 'Toko Baru1', '2', '1', 'A1', '1', 0, NULL, '1', 'temp', NULL, NULL, '2023-03-01 09:24:31', '2023-03-11 10:52:31');
INSERT INTO `posts` VALUES (8, 'asd', 'asda', '2', '2', 'A1', '1', 0, NULL, '2', 'temp', NULL, NULL, '2023-03-08 09:24:31', '2023-03-11 10:54:41');
INSERT INTO `posts` VALUES (9, 'sssss', 'sssssssssssssssssss', '2', '2', 'A1', '1', 0, NULL, '2', 'temp', NULL, NULL, '2023-03-11 09:24:31', '2023-03-12 10:22:40');
INSERT INTO `posts` VALUES (10, '\'user_id\'=>Auth()->user()->id,             \'flag\'=>\'temp\'', 'qqqq', '2', '2', 'A1', '1', 0, NULL, '2', 'temp', NULL, NULL, '2023-03-06 09:24:31', '2023-03-12 06:35:09');
INSERT INTO `posts` VALUES (12, 'Penambahan Modem Backup1', 'Penambahan Modem Backup1', '2', '2', 'A1', '1', 0, NULL, '2', 'temp', NULL, NULL, '2023-03-07 09:24:31', '2023-03-11 11:03:38');
INSERT INTO `posts` VALUES (13, 'Penambahan Modem Backup1', 'Penambahan Modem Backup1', '2', '2', 'A1', '1', 0, NULL, '2', 'temp', NULL, NULL, '2023-03-13 12:38:33', '2023-03-12 10:10:39');
INSERT INTO `posts` VALUES (14, 'Penambahan Modem Backup1', 'Penambahan Modem Backup1Penambahan Modem Backup1', '2', '2', 'A1', '1', 1, NULL, '2', 'temp', NULL, NULL, '2023-03-13 12:38:33', '2023-03-12 06:46:20');
INSERT INTO `posts` VALUES (15, 'Penambahan Modem Backup1', 'Penambahan Modem Backup1', '2', '2', 'A1', '1', 1, NULL, '2', 'temp', NULL, NULL, '2023-03-13 12:38:33', '2023-03-12 06:46:25');
INSERT INTO `posts` VALUES (16, 'update', 'update', '2', '2', 'A1', '1', 0, NULL, '3', 'temp', NULL, NULL, '2023-03-13 12:38:33', '2023-03-11 11:05:33');
INSERT INTO `posts` VALUES (17, 'update', 'update', '2', '2', 'A1', '1', 0, NULL, '3', 'temp', NULL, NULL, '2023-03-13 12:38:33', '2023-03-11 11:05:45');
INSERT INTO `posts` VALUES (18, 'zzzz', 'zzzz', '2', '2', 'A1', '1', 0, NULL, '3', 'temp', NULL, NULL, '2023-03-13 12:38:33', '2023-03-11 12:47:53');
INSERT INTO `posts` VALUES (21, 'session()->flash(\'message\', \'Disable Successfully.\');', 'session()->flash(\'message\', \'Disable Successfully.\');', '1', '1', 'A1', '1', 0, NULL, '3', 'temp', NULL, NULL, '2023-03-10 13:29:20', '2023-03-11 13:29:20');
INSERT INTO `posts` VALUES (22, 'session()->flash(\'message\', \'Disable Successfully.\');', 'session()->flash(\'message\', \'Disable Successfully.\');', '1', '1', 'A1', '1', 0, NULL, '3', 'temp', NULL, NULL, '2023-03-11 13:29:28', '2023-03-11 13:29:28');
INSERT INTO `posts` VALUES (23, 'session()->flash(\'message\', \'Disable Successfully.\');', 'session()->flash(\'message\', \'Disable Successfully.\');', '1', '1', 'A1', '1', 0, NULL, '3', 'temp', NULL, NULL, '2023-03-12 13:37:23', '2023-03-11 14:43:08');
INSERT INTO `posts` VALUES (24, '        \'status\' => \'required\',', '        \'status\' => \'required\',\n', '1', '1', 'A1', '1', 0, NULL, '3', 'temp', NULL, NULL, '2023-03-12 15:13:26', '2023-03-11 15:13:39');
INSERT INTO `posts` VALUES (25, '\'current_team_id\' => \'0\',                 \'branch_code\' => \'0\',                 \'is_admin\' => \'0\',                 \'active\' => 1,', '\'current_team_id\' => \'0\',\n                \'branch_code\' => \'0\',\n                \'is_admin\' => \'0\',\n                \'active\' => 1,', '1', '1', 'A1', '1', 0, NULL, '3', 'temp', NULL, NULL, '2023-03-12 15:24:12', '2023-03-11 15:24:22');
INSERT INTO `posts` VALUES (26, '            \'branch_code\'=>Auth()->user()->branch_code,', '            \'branch_code\'=>Auth()->user()->branch_code,\n', '1', '1', 'A1', '1', 0, NULL, '1', 'temp', NULL, NULL, '2023-03-12 05:59:32', '2023-03-12 05:59:32');
INSERT INTO `posts` VALUES (27, 'bg-green-500', 'bg-green-500', '2', '2', 'A1', '1', 0, NULL, '2', 'temp', NULL, NULL, '2023-03-12 10:10:54', '2023-03-12 10:10:54');

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint UNSIGNED NULL DEFAULT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sessions_user_id_index`(`user_id` ASC) USING BTREE,
  INDEX `sessions_last_activity_index`(`last_activity` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sessions
-- ----------------------------
INSERT INTO `sessions` VALUES ('ODgr2JH4PwzVOMgAyA1uHlAind1AIjhCtgOPvUgS', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.41', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiNTZwMlE4azc1M2xKbnQ4RzlDc0ZCMGthclNUZTdjV3NWOXRkVTNVdCI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjY6Imh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9ob21lIjt9czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTt9', 1679081287);

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_code` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pic` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `config` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `default` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `log` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_team_id` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES (1, 'A1', 'Pepito Market', 'Yogyakarta', '1', '1', '0274', '2x2.png', '11', 'info@yogya.com', 'Bantul', '1', '2x2.png', '1', '1', '2023-01-17 22:52:22', '2023-01-17 22:52:24');
INSERT INTO `settings` VALUES (2, 'A2', 'Ameya Living Style', 'Yogyakarta', '1', '1', '0274', '2x2.png', '11', 'info@yogya.com', 'Bantul', '1', '2x2.png', '3', '1', '2023-01-17 22:52:22', '2023-01-17 22:52:24');
INSERT INTO `settings` VALUES (3, 'A3', 'WFX', 'Yogyakarta', '1', '1', '0274', '2x2.png', '11', 'info@yogya.com', 'Bantul', '1', '2x2.png', '2', '1', '2023-01-17 22:52:22', '2023-01-17 22:52:24');

-- ----------------------------
-- Table structure for team_invitations
-- ----------------------------
DROP TABLE IF EXISTS `team_invitations`;
CREATE TABLE `team_invitations`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `team_id` bigint UNSIGNED NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `team_invitations_team_id_email_unique`(`team_id` ASC, `email` ASC) USING BTREE,
  CONSTRAINT `team_invitations_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of team_invitations
-- ----------------------------

-- ----------------------------
-- Table structure for team_user
-- ----------------------------
DROP TABLE IF EXISTS `team_user`;
CREATE TABLE `team_user`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `team_id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `team_user_team_id_user_id_unique`(`team_id` ASC, `user_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of team_user
-- ----------------------------

-- ----------------------------
-- Table structure for teams
-- ----------------------------
DROP TABLE IF EXISTS `teams`;
CREATE TABLE `teams`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_team` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `teams_user_id_index`(`user_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teams
-- ----------------------------
INSERT INTO `teams` VALUES (1, 1, 'Haris\'s Team', 1, '2023-03-11 07:37:37', '2023-03-11 07:37:37');
INSERT INTO `teams` VALUES (2, 2, 'IT\'s Team', 1, '2023-03-11 10:21:14', '2023-03-11 10:21:14');
INSERT INTO `teams` VALUES (3, 1, 'Haris\'s Team', 1, '2023-03-11 15:05:05', '2023-03-11 15:05:05');
INSERT INTO `teams` VALUES (4, 2, 'IT\'s Team', 1, '2023-03-11 15:18:30', '2023-03-11 15:18:30');
INSERT INTO `teams` VALUES (5, 3, 'Haris\'s Team', 1, '2023-03-11 15:23:02', '2023-03-11 15:23:02');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nik` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `newpassword` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `current_team_id` bigint UNSIGNED NULL DEFAULT NULL,
  `profile_photo_path` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `branch_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_admin` tinyint(1) NULL DEFAULT 0,
  `active` int UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username` ASC) USING BTREE,
  UNIQUE INDEX `users_phone_unique`(`phone` ASC) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email` ASC) USING BTREE,
  UNIQUE INDEX `users_nik_unique`(`nik` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Haris Rifai', 'mac0285', '08179730808', 'moh.h.rifai@gmail.com', '1205013', NULL, '$2y$10$Rsm8xOSLCFfKswkzJNhYiO5EjILmQGW2mLu7CDxb5Fa7sYye46TbO', NULL, 'blb1oFYHRny0YiJDDjf1TCWcn2DLpZu4ZXDowPQGk2vQXBKTTMDzTAKeL5fC', 1, 'user.png', 'A1', 1, 1, '2023-03-11 15:05:05', '2023-03-11 15:05:05');
INSERT INTO `users` VALUES (2, 'IT Support', 'timtik', '22179730808', 'admin@admin.com', '1205012', NULL, '$2y$10$DwTTx8k/NYYpiMGXqbGsTuYu/bU3gVdDwyQtoGM9806Y.8LLn2H.K', '123123123', '6lH8HnGLpiaPpxLbPZCxKLiCdTpWyfaV2bhBmhf2c3pMtcR5BkN5B3b0oXmL', 2, 'user.png', 'A1', 0, 1, '2023-03-11 15:18:30', '2023-03-11 15:18:30');
INSERT INTO `users` VALUES (3, 'Data Warehouse', 'root', '08179730823', 'muhammad.rifai@metrocom.co.id', '1205014', NULL, '$2y$10$7mc87VPSD/inO7wb0zYifuDei8QHixPiuZZUhtgyDgHZtuDIg0BJW', '123123qwe', NULL, 0, 'user.png', 'A1', 0, 1, '2023-03-11 15:23:02', '2023-03-11 15:23:02');

SET FOREIGN_KEY_CHECKS = 1;
