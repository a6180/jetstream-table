<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Assetkita
 *
 * @property int $id
 * @property string $ip_comp
 * @property string $hostname_comp
 * @property string|null $userpc
 * @property string|null $os_comp
 * @property string|null $type_comp
 * @property string|null $processor_comp
 * @property string|null $ram_comp
 * @property string|null $hdd_comp
 * @property string|null $brand
 * @property string|null $dept_comp
 * @property string|null $branch_code
 * @property string|null $flag
 * @property string|null $photo
 * @property string|null $file_name
 * @property string|null $remote
 * @property string|null $remark
 * @property string|null $current_team_id
 * @property string|null $active
 * @property string|null $buy_at
 * @property string|null $delete_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita query()
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereBranchCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereBuyAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereCurrentTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereDeleteAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereDeptComp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereHddComp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereHostnameComp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereIpComp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereOsComp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereProcessorComp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereRamComp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereRemote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereTypeComp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assetkita whereUserpc($value)
 */
	class Assetkita extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Branchoffice
 *
 * @property int $id
 * @property string $branch_code
 * @property string $branch
 * @property string $type
 * @property string $l1
 * @property string $l2
 * @property string $l3
 * @property string $build_date
 * @property string $opening_date
 * @property string $address1
 * @property string $address2
 * @property string $address3
 * @property string $phone_code
 * @property string $phone_land
 * @property string $mail_address
 * @property string $npwp
 * @property string $city
 * @property string $postal_code
 * @property string $province
 * @property string $country
 * @property string $remark
 * @property string $current_team_id
 * @property string $active
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice query()
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereAddress1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereAddress3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereBranch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereBranchCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereBuildDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereCurrentTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereL1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereL2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereL3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereMailAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereNpwp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereOpeningDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice wherePhoneCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice wherePhoneLand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Branchoffice withoutTrashed()
 */
	class Branchoffice extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Departement
 *
 * @property int $id
 * @property string $group
 * @property string $dept
 * @property string $current_team_id
 * @property string $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Departement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Departement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Departement query()
 * @method static \Illuminate\Database\Eloquent\Builder|Departement whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Departement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Departement whereCurrentTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Departement whereDept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Departement whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Departement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Departement whereUpdatedAt($value)
 */
	class Departement extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Email
 *
 * @property int $id
 * @property string|null $name_usr
 * @property string|null $email_usr
 * @property string|null $pwd_usr
 * @property string|null $email_type
 * @property string|null $dept_usr
 * @property string|null $remark_usr
 * @property string|null $month_date
 * @property string|null $branch_code
 * @property string|null $current_team_id
 * @property string|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Email newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Email newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Email query()
 * @method static \Illuminate\Database\Eloquent\Builder|Email whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Email whereBranchCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Email whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Email whereCurrentTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Email whereDeptUsr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Email whereEmailType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Email whereEmailUsr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Email whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Email whereMonthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Email whereNameUsr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Email wherePwdUsr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Email whereRemarkUsr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Email whereUpdatedAt($value)
 */
	class Email extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Membership
 *
 * @property int $id
 * @property int $team_id
 * @property int $user_id
 * @property string|null $role
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Membership newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Membership newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Membership query()
 * @method static \Illuminate\Database\Eloquent\Builder|Membership whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Membership whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Membership whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Membership whereTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Membership whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Membership whereUserId($value)
 */
	class Membership extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Passwordmgr
 *
 * @property int $id
 * @property string|null $address
 * @property string|null $user
 * @property string|null $pass
 * @property string|null $type
 * @property string|null $remark
 * @property string|null $branch_code
 * @property string|null $current_team_id
 * @property string|null $active
 * @property string|null $status
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr query()
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr whereBranchCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr whereCurrentTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr wherePass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passwordmgr whereUser($value)
 */
	class Passwordmgr extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Post
 *
 * @property int $id
 * @property string $title
 * @property string $desc
 * @property string|null $dept_id
 * @property string|null $current_team_id
 * @property string|null $branch_code
 * @property string|null $active
 * @property int|null $status
 * @property string|null $remark
 * @property string|null $user_id
 * @property string|null $flag
 * @property string|null $date_at
 * @property string|null $date_close
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereBranchCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCurrentTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDateAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDateClose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDeptId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUserId($value)
 */
	class Post extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Settings
 *
 * @property int $id
 * @property string $branch_code
 * @property string $name
 * @property string $factory
 * @property string $telp
 * @property string $email
 * @property string $address
 * @property string $pic
 * @property string $config
 * @property string $default
 * @property string $location
 * @property string $flag
 * @property string $log
 * @property string $current_team_id
 * @property string $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Settings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Settings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Settings query()
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereBranchCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereConfig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereCurrentTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereFactory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereLog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings wherePic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereTelp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereUpdatedAt($value)
 */
	class Settings extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Team
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property bool $personal_team
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User|null $owner
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\TeamInvitation> $teamInvitations
 * @property-read int|null $team_invitations_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $users
 * @property-read int|null $users_count
 * @method static \Database\Factories\TeamFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Team newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Team newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Team query()
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team wherePersonalTeam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereUserId($value)
 */
	class Team extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TeamInvitation
 *
 * @property int $id
 * @property int $team_id
 * @property string $email
 * @property string|null $role
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Team $team
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation query()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation whereTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation whereUpdatedAt($value)
 */
	class TeamInvitation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $phone
 * @property string $email
 * @property string $nik
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $newpassword
 * @property string|null $remember_token
 * @property int|null $current_team_id
 * @property string|null $profile_photo_path
 * @property string|null $branch_code
 * @property int|null $is_admin
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Rappasoft\LaravelAuthenticationLog\Models\AuthenticationLog> $authentications
 * @property-read int|null $authentications_count
 * @property-read \App\Models\Team|null $currentTeam
 * @property-read \Rappasoft\LaravelAuthenticationLog\Models\AuthenticationLog|null $latestAuthentication
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection<int, \Illuminate\Notifications\DatabaseNotification> $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Team> $ownedTeams
 * @property-read int|null $owned_teams_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Team> $teams
 * @property-read int|null $teams_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Laravel\Sanctum\PersonalAccessToken> $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBranchCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCurrentTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereNewpassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereNik($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProfilePhotoPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUsername($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\issuesbank
 *
 * @property int $id
 * @property string|null $module
 * @property string|null $detail
 * @property string|null $root_cause
 * @property string|null $solutions
 * @property string|null $changefile
 * @property string|null $changedb
 * @property string|null $changehw
 * @property string|null $status
 * @property string|null $reportby
 * @property string|null $pic
 * @property string|null $remark
 * @property string|null $root_date
 * @property string|null $fix_date
 * @property string|null $branch_code
 * @property string|null $current_team_id
 * @property string|null $active
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank query()
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereBranchCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereChangedb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereChangefile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereChangehw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereCurrentTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereFixDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereModule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank wherePic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereReportby($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereRootCause($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereRootDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereSolutions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|issuesbank whereUpdatedAt($value)
 */
	class issuesbank extends \Eloquent {}
}

