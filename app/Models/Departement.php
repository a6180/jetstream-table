<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Departement extends Model
{
    use HasFactory;
    protected $guarded  = [];
    //public $primarykey='id';
    protected $table = "departements";
    protected $fillable = ['id',
    'group',
    'dept',
    'current_team_id',
    'active'
    ];
}
