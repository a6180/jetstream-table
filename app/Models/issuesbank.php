<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class issuesbank extends Model
{
    use HasFactory;
    protected $guarded  = [];
    protected $fillable = [
                'module',
                'detail',
                'root_cause',
                'solutions',
                'changefile',
                'changedb',
                'changehw',
                'status',
                'reportby',
                'pic',
                'remark',
                'root_date',
                'fix_date',
                'branch_code',
                'current_team_id',
                'active'
];
}
