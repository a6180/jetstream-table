<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $guarded  = [];
    protected $fillable = ['title','desc','dept_id', 'current_team_id','branch_code','active','status','user_id','remark','flag','date_at','date_close','created_at'
    ];
}
