<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Rappasoft\LaravelAuthenticationLog\Traits\AuthenticationLoggable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class contact extends Model
{
    use HasFactory;
    use Notifiable, AuthenticationLoggable;
    protected $guarded  = [];
    protected $fillable = [
            'renewal_date',
            'namedisplay',
            'group',
            'dept',
            'lantai',
            'extnumber',
            'digital',
            'mail',
            'remote',
            'branch_code',
            'current_team_id',
            'active',
            'deleted_at'  ];

}
