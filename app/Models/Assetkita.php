<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Assetkita extends Model
{
    use HasFactory;
    protected $guarded  = [];
    protected $fillable = [
    'ip_comp',
    'hostname_comp',
    'userpc',
    'os_comp',
    'type_comp',
    'processor_comp',
    'ram_comp',
    'hdd_comp',
    'brand',
    'dept_comp',
    'branch_code',
    'flag',
    'photo',
    'file_name',
    'remote',
    'remark',
    'current_team_id',
    'active',
    'buy_at',
    'delete_at'

    ];

    public $timestamps = true;

}
