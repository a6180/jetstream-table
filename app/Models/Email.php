<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Rappasoft\LaravelAuthenticationLog\Traits\AuthenticationLoggable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Email extends Model
{
    use HasFactory;
    use Notifiable, AuthenticationLoggable;
    protected $guarded  = [];
    protected $fillable = [
            'name_usr',
            'email_usr',
            'pwd_usr',
            'email_type',
            'dept_usr',
            'remark_usr',
            'month_date',
            'branch_code',
            'current_team_id',
            'active'
        ];
}
