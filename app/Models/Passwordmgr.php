<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Passwordmgr extends Model
{
    use HasFactory;
    protected $guarded  = [];
    protected $fillable = [
                        'address',
                        'user',
                        'pass',
                        'type',
                        'remark',
                        'branch_code',
                        'current_team_id',
                        'active',
                        'status',
                        'deleted_at'
    ];
}
