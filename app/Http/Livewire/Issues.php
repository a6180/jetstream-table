<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Departement;
use App\Models\issuesbank;

use Livewire\WithPagination;
use Carbon\Carbon;

class Issues extends Component
{
    public $issues_id, $module, $detail, $root_cause, $solutions, $changefile,$changedb,
    $changehw, $status,$reportby,$pic,$remark,$root_date,$fix_date,$branch_code,
    $current_team_id,$active;
    use WithPagination;
    public $search = '';
    public $searchTerm;
    public $q;
    public $sortBy = 'module';
    public $sortAsc = true;
    public $item;
    public $isModalOpen = 0;
    public $isModalEdit = 0;
    public function updatingSearch()
    {
        $this->resetPage();
    }
    public function render()
    {

    $deptissue= Departement::SELECT('id','group', 'dept')
        ->where('active', 1)->where('current_team_id', auth()->user()->current_team_id)->get();
    $issues = issuesbank::select('id','module','detail','root_cause','solutions','changefile','changedb','changehw','status', 'reportby',
    'pic','remark','root_date','fix_date','branch_code','current_team_id','active')
    ->where('current_team_id', auth()->user()->current_team_id)
    ->where('branch_code', auth()->user()->branch_code)
    ->where('active', '1')
    ->when( $this->q, function($query) {
     return $query->where(function( $query) {
     $query->where('module', 'like', '%'.$this->q. '%')
           ->orWhere('detail', 'like', '%' .$this->q. '%')
           ->orWhere('root_cause', 'like', '%' .$this->q. '%')
           ->orWhere('reportby', 'like', '%' .$this->q. '%')
           ->orWhere('status', 'like', '%' .$this->q. '%')
           ->orWhere('solutions', 'like', '%' .$this->q. '%');
            });
     })->when($this->active, function( $query) {
            return $query->active();
     })->orderBy( $this->sortBy, $this->sortAsc ? 'ASC' : 'DESC');
     $issues = $issues->paginate(5);
        return view('livewire.issues.view', [
            'issues' => $issues,'deptissue'=>$deptissue]);
    }
    private function resetCreateForm(){
        $this->module= '';
        $this->detail= '';
        $this->root_cause= '';
        $this->solutions= '';
        $this->changefile= '';
        $this->changedb= '';
        $this->changehw= '';
        $this->status= '';
        $this->reportby= '';
        $this->pic= '';
        $this->remark= '';
        $this->root_date= '';
        $this->fix_date= '';

    }
    public function sortBy( $field)
    {
        if( $field == $this->sortBy) {
            $this->sortAsc = !$this->sortAsc;
        }
        $this->sortBy = $field;
    }
    public function markAsHide(issuesbank $item)
    {
        $item->active = 0;
        $item->save();
    }
    public function markAsDisable(issuesbank $item)
    {
        $item->active = false;
        $item->save();
        session()->flash('message', 'Disable Successfully.');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isModalOpen = true;
    }
    public function openModalEdit()
    {
        $this->isModalEdit = true;
    }
    public function closeModal()
    {
        $this->isModalOpen = false;
    }
    public function closeModalEdit()
    {
        $this->isModalEdit = false;
    }
    public function edit($id)
    {
        $issues = issuesbank::findOrFail($id);
        $this->issues_id = $id;
        $this->module=$issues           ->module;
        $this->detail=$issues           ->detail;
        $this->root_cause=$issues       ->root_cause;
        $this->solutions=$issues        ->solutions;
        $this->changefile=$issues       ->changefile;
        $this->changedb=$issues         ->changedb;
        $this->changehw=$issues         ->changehw;
        $this->status=$issues           ->status;
        $this->reportby=$issues         ->reportby;
        $this->pic=$issues              ->pic;
        $this->remark=$issues           ->remark;
        $this->root_date=$issues        ->root_date;
        $this->fix_date=$issues         ->fix_date;
       // $this->branch_code=$issues      ->branch_code;
       // $this->current_team_id=$issues  ->current_team_id;
       // $this->active=$issues           ->active;

        //$this->active= $usages->active;
        $this->openModalEdit();
    }

    public function storeEdit()
    {
        $validatedDate = $this->validate([
            'module'        => 'required|max:25',
            'detail'        => 'required',
            'root_cause'    => 'required',
            'solutions'     => 'required',
            'changefile'    => 'required',
            'changedb'      => 'required',
            'changehw'      => 'required',
            'status'      => 'required',
            //'dept_usr' => 'required',

        ]);

        if ($this->issues_id) {
            $issues = issuesbank::find($this->issues_id);
            $issues->update([
            'module' => $this->module,
            'detail' => $this->detail,
            'root_cause' => $this->root_cause,
            'solutions' => $this->solutions,
            'changefile' => $this->changefile,
            'changedb' => $this->changedb,
            'changehw' => $this->changehw,
            'status' => '1',
            'reportby'=>Auth()->user()->name,
            'pic' => $this->pic,
            'remark' => $this->remark,
            'root_date' => Carbon::now(),
            'fix_date' => Carbon::now(),
            'branch_code' =>Auth()->user()->branch_code,
            'current_team_id'=>Auth()->user()->current_team_id,
            'status'=>$this->status,
            'active'=>true
            ]);

            session()->flash('message', 'Email Updated Successfully.');
            $this->resetCreateForm();
            $this->closeModalEdit();

        }
    }
    public function store()
    {
        $this->validate([
            'module' => 'required|max:25',
            'detail' => 'required',
            'root_cause' => 'required',
           // 'solutions' => 'required',
           // 'changefile'=> 'required',
           // 'changedb'=> 'required',
           // 'changehw' => 'required',

        ]);
       // $nextbuy=Carbon::create(order_date);
       issuesbank::Create([
        'module' => $this->module,
        'detail' => $this->detail,
        'root_cause' => $this->root_cause,
       // 'solutions' => $this->solutions,
       // 'changefile' => $this->changefile,
       // 'changedb' => $this->changedb,
       // 'changehw' => $this->changehw,
        'status' => '1',
        'reportby'=>Auth()->user()->name,
        'pic' => $this->pic,
        'remark' => $this->remark,
        'root_date' => Carbon::now(),
        'fix_date' => Carbon::now(),
        'branch_code' =>Auth()->user()->branch_code,
        'current_team_id'=>Auth()->user()->current_team_id,
        'status'=>'0',
        'active'=>true

        ]);

        session()->flash('message', 'Data added successfully.');
        $this->resetCreateForm();
        $this->closeModal();
    }

}
