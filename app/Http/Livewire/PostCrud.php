<?php

namespace App\Http\Livewire;
use Livewire\WithPagination;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

use App\Models\Post;
class PostCrud extends Component
{

    public $title, $desc, $post_id, $date_at, $active;
    public $isModalOpen = 0;
    public $isModalUpdate = 0;

    public $status;
    public $field;
    use WithPagination;
    public $q;
    public $search = '';
    public $searchTerm;
     public $sortBy = 'created_at';
    public $sortAsc = true;
    public $item;
    public function updatingSearch()
    {
        $this->resetPage();
    }
    public function sortBy( $field)
    {
        if( $field == $this->sortBy) {
            $this->sortAsc = !$this->sortAsc;
        }
        $this->sortBy = $field;
    }

    public function render()
    {
        $posts = Post::SELECT('id','title', 'desc','active','current_team_id','status','remark','date_at','date_close','created_at')
        ->where('active', 1)
        ->where('current_team_id', auth()->user()->current_team_id)
        ->where('user_id', auth()->user()->id)
        ->where('branch_code', auth()->user()->branch_code)
        ->whereYear('created_at', '=', Carbon::now()->year)
       // ->latest('date_at')->simplePaginate(5);
        //$posts = $this->paginate(15);
       // return view('livewire.posting.view', ['posts' => $posts]);

        ->when( $this->q, function($query) {
            return $query->where(function( $query) {
                $query->where('title', 'like', '%'.$this->q. '%')
                    ->orWhere('desc', 'like', '%' .$this->q. '%')
                    ->orWhere('remark', 'like', '%' .$this->q. '%');
            });
        })->when($this->active, function( $query) {
            return $query->active();
       // })->orderByRaw('dept_comp - created_at DESC');
    })->orderBy( $this->sortBy, $this->sortAsc ? 'ASC' : 'DESC');

    $posts = $posts->paginate(5);
    return view('livewire.posting.view', [
        'posts' => $posts
    ]);
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isModalOpen = true;
    }
    public function updateModal()
    {
        $this->isModalUpdate = true;
    }

    public function closeModal()
    {
        $this->isModalOpen = false;
    }
    public function updatecloseModal()
    {
        $this->isModalUpdate = false;
    }

    private function resetCreateForm(){
        $this->title = '';
        $this->desc = '';
    }
    public function store()
    {
        $this->validate([
            'title' => 'required|max:25',
            'desc' => 'required',
            'date_at' => 'required',
           // 'remark' => 'required',
        ]);

        Post::Create([
            'title' => $this->title,
            'desc' => $this->desc,
            'dept_id'=>Auth()->user()->current_team_id,
            'current_team_id'=>Auth()->user()->current_team_id,
            'branch_code'=>Auth()->user()->branch_code,
            'active'=>'1',
            'status'=> '0',
            'user_id'=>Auth()->user()->id,
            'flag'=>'temp',
            'remark'=>'0',
            'date_at'=> $this->date_at,
            'date_close'=>Carbon::now()->subDays(5),
        ]);

        session()->flash('message', $this->post_id ? 'Data Created successfully.' : 'Data added successfully.');
        $this->resetCreateForm();
        $this->closeModal();
    }
    public function storeEdit()
    {
        $this->validate([
        // Execution doesn't reach here if validation fails.
        'title' => 'required|max:25',
        'desc' => 'required',
        'status' => 'required',
        'remark' => 'required | min:8',
        //'date_close' => 'required',
        ]);
        if ($this->post_id) {
            $record = Post::find($this->post_id);
            $record->update([
            //Post::Create(['id' => $this->post_id], [
                'title' => $this->title,
                'desc' => $this->desc,
                'dept_id'=>Auth()->user()->current_team_id,
                'current_team_id'=>Auth()->user()->current_team_id,
                'branch_code'=>Auth()->user()->branch_code,
                'active'=>'1',
                'remark'=>$this->remark,
                'status'=> $this->status,
                'user_id'=>Auth()->user()->id,
                'flag'=>'temp',
                'date_close' =>Carbon::now()
            ]);

            $this->resetCreateForm();
            //$this->updateModel = false;
        }
        session()->flash('message', $this->post_id ? 'Data updated successfully.' : 'Data added successfully.');
        $this->resetCreateForm();
        $this->updatecloseModal();
    }
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $this->post_id = $id;
        $this->title = $post->title;
        $this->desc = $post->desc;
        $this->remark = $post->remark;
        $this->updateModal();
    }
    public function delete(Post $id)
    {
        //$item->active = "Close";
        $id->active = 0;
        $id->save();
        session()->flash('message', 'Disable Successfully.');
    }
}
