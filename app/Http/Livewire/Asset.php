<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Livewire\WithPagination;
use Carbon\Carbon;
use App\Models\Assetkita;
use App\Models\Departement;

class Asset extends Component
{
    public $id_komp, $ip_comp, $userpc,$hostname_comp,$os_comp,$type_comp, $processor_comp,$brand,
    $ram_comp,$hdd_comp,$ups,$office_actv,$antivir,$dept_comp,$buy_at, $destroy_at, $flag, $photo,$file_name, $branch_code,$current_team_id,$active,$remark,$remote;
    public $isModalOpen = 0;
    public $isModalUpdate = 0;
    public $status;
    public $field;
    use WithPagination;
    public $q;
    public $search = '';
    public $searchTerm;

    public $sortBy = 'dept_comp';
    public $sortAsc = true;
    public $item;
    public function updatingSearch()
    {
        $this->resetPage();
    }
    public function sortBy( $field)
    {
        if( $field == $this->sortBy) {
            $this->sortAsc = !$this->sortAsc;
        }
        $this->sortBy = $field;
    }
    public function render()
    {
        $deptcomp= Departement::select('id','group', 'dept')
        ->where('active', 1)->where('current_team_id', auth()->user()->current_team_id)->get();
        $assets = Assetkita::SELECT('id','ip_comp','hostname_comp', 'userpc', 'os_comp', 'type_comp','processor_comp', 'ram_comp',
        'hdd_comp', 'brand','dept_comp','branch_code','flag','photo','file_name', 'remote',
        'remark', 'current_team_id','active','buy_at','created_at','delete_at')
        ->where('active', 1)
        ->where('current_team_id', auth()->user()->current_team_id)
        ->where('branch_code', auth()->user()->branch_code)
       // ->latest('buy_at')->Paginate(5);
       // return view('livewire.infra.view', ['assets' => $assets]);
        ->when( $this->q, function($query) {
            return $query->where(function( $query) {
                $query->where('ip_comp', 'like', '%'.$this->q. '%')
                    ->orWhere('hostname_comp', 'like', '%' .$this->q. '%')
                    ->orWhere('os_comp', 'like', '%' .$this->q. '%')
                    ->orWhere('type_comp', 'like', '%' .$this->q. '%')
                    ->orWhere('dept_comp', 'like', '%' .$this->q. '%')
                    ->orWhere('userpc', 'like', '%' .$this->q. '%');
            });
        })->when($this->active, function( $query) {
            return $query->active();
       // })->orderByRaw('dept_comp - created_at DESC');
    })->orderBy( $this->sortBy, $this->sortAsc ? 'ASC' : 'DESC');

    $assets = $assets->paginate(5);
    return view('livewire.infra.view', [
        'assets' => $assets,'deptcomp'=>$deptcomp
    ]);

      // dd($assets);
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isModalOpen = true;
    }
    public function updateModal()
    {
        $this->isModalUpdate = true;
    }

    public function closeModal()
    {
        $this->isModalOpen = false;
    }
    public function updatecloseModal()
    {
        $this->isModalUpdate = false;
    }

    private function resetCreateForm(){
        $this->ip_comp= '';
        $this->userpc= '';
        $this->hostname_comp= '';
        $this->os_comp= '';
        $this->brand= '';
        $this->type_comp= '';
        $this->processor_comp= '';
        $this->ram_comp= '';
        $this->hdd_comp= '';
        $this->dept_comp= '';
        $this->buy_at= '';
        $this->destroy_at= '';
        $this->flag= '';
        $this->photo= '';
        $this->file_name= '';
        $this->current_team_id= '';
        //$this->active= '';
    }

    public function storeEdit()
    {

        $this->validate([
        // Execution doesn't reach here if validation fails.
        'ip_comp'=>'required|max:25',
        'hostname_comp'=>'required',
        'dept_comp'=>'required|min:1',
        //'buy_at'=>'required',
        'os_comp'=>'required|min:1',
        'brand'=>'required|min:1',
        'userpc'=>'required|min:1',
        'type_comp'=>'required',
        'processor_comp'=>'required',
        'ram_comp'=>'required|string|min:1|max:3',
        'hdd_comp'=>'required|string|min:1|max:3',
        'remote'=>'required|min:1',
        'remark'=>'required|max:50'

        ]);

        if ($this->id_komp) {
            $record = Assetkita::find($this->id_komp);
            $record->update([
            'ip_comp'=> $this->ip_comp,
            'hostname_comp'=> $this->hostname_comp,
            'userpc'=> $this->userpc,
            'os_comp'=> $this->os_comp,
            'type_comp'=> $this->type_comp,
            'processor_comp'=> $this->processor_comp,
            'ram_comp'=> $this->ram_comp,
            'hdd_comp'=> $this->hdd_comp,
            'brand'=> $this->brand,
            'dept_comp'=>$this->dept_comp,
            'branch_code'=>Auth()->user()->branch_code,
            'flag'=> '2',
            'photo'=> '1',
            'file_name'=> '1.jpg',
            'remote'=> $this->remote,
            'remark'=> $this->remark,
            'current_team_id'=>Auth()->user()->current_team_id,
            'active'=>'1',
            //'buy_at'=>Carbon::now(),
            //'delete_at'=>Carbon::now()
            ]);

           // dd($record);
        }


            session()->flash('message', 'Usages Updated Successfully.');
            $this->resetCreateForm();
            $this->updatecloseModal();


    }

    public function store()
    {
        $this->validate([
            // Execution doesn't reach here if validation fails.
            'ip_comp'=>'required',
            'hostname_comp'=>'required',
            'buy_at'=>'required',
            'os_comp'=>'required|min:1',
            'brand'=>'required|min:1',
            'userpc'=>'required|min:1',
            'type_comp'=>'required',
            'processor_comp'=>'required',
            'ram_comp'=>'required|string|min:1|max:3',
            'hdd_comp'=>'required|string|min:1|max:3',
            'dept_comp'=>'required|string|min:1'
            //'remote'=>'required|min:1'
            ]);
          //  'required|string|min:1|max:3',

        Assetkita::Create([
            'ip_comp'=> $this->ip_comp,
            'hostname_comp'=> $this->hostname_comp,
            'userpc'=> $this->userpc,
            'os_comp'=> $this->os_comp,
            'type_comp'=> $this->type_comp,
            'processor_comp'=> $this->processor_comp,
            'ram_comp'=> $this->ram_comp,
            'hdd_comp'=> $this->hdd_comp,
            'brand'=> $this->brand,
            'dept_comp'=>$this->dept_comp,
            'branch_code'=>Auth()->user()->branch_code,
            'flag'=> '1',
            'photo'=> '1',
            'file_name'=> '1',
            'remote'=> '0',
            'remark'=> '0',
            'current_team_id'=>Auth()->user()->current_team_id,
            'active'=>'1',
            'buy_at'=>$this->buy_at,
           // 'delete_at'=>Carbon::now()
     ]);

        session()->flash('message', $this->id ? 'Data Komputer updated.' : 'Data Komputer created.');
        $this->resetCreateForm();
        $this->closeModal();

    }

    public function edit($id)
    {
        $assets = Assetkita::findOrFail($id);
        $this->id_komp = $id;
        $this->ip_comp=$assets->ip_comp;
        $this->userpc=$assets->userpc;
        $this->hostname_comp= $assets->hostname_comp;
        $this->os_comp= $assets->os_comp;
        $this->type_comp= $assets->type_comp;
        $this->processor_comp= $assets->processor_comp;
        $this->ram_comp= $assets->ram_comp;
        $this->hdd_comp= $assets->hdd_comp;
        $this->brand= $assets->brand;
       // $this->buy_at= $assets->buy_at;

        $this->dept_comp= $assets->dept_comp;
       // 'branch_code'=>Auth()->user()->branch_code,
        $this->branch_code= $assets->branch_code;
        $this->current_team_id= $assets->current_team_id;
        $this->remark= $assets->remark;
        $this->remote= $assets->remote;
        //$this->active= $komputer->active;
        $this->updateModal();
    }

}
