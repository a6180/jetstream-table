<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Livewire\Component;
use Illuminate\Http\Request;
use Redirect,Response;
Use DB;
use Carbon\Carbon;
class HomeCharts extends Component
{


  public function render()
    {


       // $users = Post::raw("COUNT(*) as count")
       //$users  = Post::where('current_team_id', auth()->user()
       //->current_team_id)->where('branch_code', auth()->user()
       //->branch_code)->where('active', 1)
       //->count();

        //$labels = $users->keys();
        //$data = $users->values();
        $record = Post::select(\DB::raw("COUNT(*) as count"), \DB::raw("MONTHNAME(created_at) as user"), \DB::raw("DAYNAME(created_at) as day_name"), \DB::raw("DAY(created_at) as day"))
                ->where('current_team_id', auth()->user()->current_team_id)->where('branch_code', auth()->user()->branch_code)->where('active', 1)
                ->where('user_id', auth()->user()->id)
                ->groupBy('user_id','user','created_at','day')
                ->where('created_at', '>', Carbon::today()->subDay(7))
                ->orderBy('day')
                ->get();
        $data = [];

     foreach($record as $row) {
        $data['label'][] = $row->day_name;
        $data['data'][] = (int) $row->count;
      }

    $data['chart_data'] = json_encode($data);
    return view('livewire.home-charts', $data);


        //return view('chart', compact('labels', 'data'));
       // return view('livewire.home-charts');
    }
}
