<?php

namespace App\Http\Livewire;

use App\Models\Assetkita;
use App\Models\contact;
use App\Models\Passwordmgr;
use App\Models\Post;
use App\Models\Email;
use App\Models\Settings;
use Livewire\Component;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class Home extends Component
{
    public function render()
    {

        $Settings=Settings::SELECT('name','location','address')->where('branch_code', auth()->user()->branch_code)->where('current_team_id', auth()->user()->current_team_id)->where('active', 1)->get();
        $Postsx = count($shows = Post::where('current_team_id', auth()->user()->current_team_id)->where('active', 1)->where('user_id', auth()->user()->id)->whereYear('created_at', '=', Carbon::now()->year)->get());
        $assetsx = count($assets = Assetkita::where('current_team_id', auth()->user()->current_team_id)->where('active', 1)->get());
        $passwordx = count($password = Passwordmgr::where('current_team_id', auth()->user()->current_team_id)->where('branch_code', auth()->user()->branch_code)->where('active', 1)->get());
        $emailsx=count($emails = Email::where('current_team_id', auth()->user()->current_team_id)->where('branch_code', auth()->user()->branch_code)->where('active', 1)->get());
        $Extensionx = count($Extension = contact::where('current_team_id', auth()->user()->current_team_id)->where('branch_code', auth()->user()->branch_code)->where('active', 1)->get());
        $IssuesBankx = count($issuesbank = contact::where('current_team_id', auth()->user()->current_team_id)->where('branch_code', auth()->user()->branch_code)->where('active', 1)->get());

        return view('livewire.home',compact('Settings','Postsx','assetsx','passwordx','emailsx','Extensionx','IssuesBankx'));
    }
}
