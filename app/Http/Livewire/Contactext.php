<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Departement;
use App\Models\contact;

use Livewire\WithPagination;
use Carbon\Carbon;


class Contactext extends Component
{
    public $depts,$contact_id,$renewal_date,$namedisplay,$group,$dept,$lantai,$extnumber, $digital,$mail,$remote,$branch_code,$current_team_id, $active,$deleted_at;
    use WithPagination;
    public $search = '';
    public $searchTerm;
    public $q;
    public $sortBy = 'dept';
    public $sortAsc = true;
    public $item;
    public $isModalOpen = 0;
    public $isModalEdit = 0;
    public function updatingSearch()
    {
        $this->resetPage();
    }
    public function render()
    {

        $deptxx= Departement::select('id','group', 'dept')
        ->where('active', 1)->where('current_team_id', auth()->user()->current_team_id)->get();
    $Emails = contact::select('id','renewal_date','namedisplay',  'group','dept','lantai','extnumber', 'digital','mail','remote', 'branch_code','current_team_id', 'active' )
    ->where('current_team_id', auth()->user()->current_team_id)
    ->where('active', 1)
    ->when( $this->q, function($query) {
        return $query->where(function( $query) {
            $query->where('namedisplay', 'like', '%'.$this->q. '%')
                ->orWhere('dept', 'like', '%' .$this->q. '%')
                ->orWhere('group', 'like', '%' .$this->q. '%')
                ->orWhere('extnumber', 'like', '%' .$this->q. '%')
                ->orWhere('digital', 'like', '%' .$this->q. '%');
        });
     })->when($this->active, function( $query) {
            return $query->active();
     })->orderBy( $this->sortBy, $this->sortAsc ? 'ASC' : 'DESC');
     $Emails = $Emails->paginate(5);
       // return view('livewire.extension.view', [
         //   'Emails' => $Emails,'dept'=>$dept,]);

         return view('livewire.extension.view',compact('deptxx','Emails'));
    }
    private function resetCreateForm(){
        $this->extnumber = '';
        $this->namedisplay = '';
        $this->group = '';
        $this->dept = '';
        $this->remote = '';
        $this->lantai = '';
    }
    public function sortBy( $field)
    {
        if( $field == $this->sortBy) {
            $this->sortAsc = !$this->sortAsc;
        }
        $this->sortBy = $field;
    }
    public function markAsHide(contact $item)
    {
        $item->active = 0;
        $item->save();
    }
    public function markAsDisable(contact $item)
    {
        $item->active = false;
        $item->save();
        session()->flash('message', 'Disable Successfully.');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isModalOpen = true;
    }
    public function openModalEdit()
    {
        $this->isModalEdit = true;
    }
    public function closeModal()
    {
        $this->isModalOpen = false;
    }
    public function closeModalEdit()
    {
        $this->isModalEdit = false;
    }
    public function edit($id)
    {
        $contacts = Contact::findOrFail($id);
        $this->contact_id = $id;
        $this->extnumber=$contacts->extnumber;
        $this->namedisplay=$contacts->namedisplay;
        $this->group=$contacts->group;
        $this->digital=$contacts->digital;
        $this->mail=$contacts->mail;
        $this->dept=$contacts->dept;
        $this->lantai=$contacts->lantai;
        $this->remote=$contacts->remote;
        $this->current_team_id= $contacts->current_team_id;
        //$this->active= $usages->active;
        $this->openModalEdit();
    }

    public function storeEdit()
    {
        $validatedext =$this->validate([
            'namedisplay' => 'required',
            'lantai' => 'required|string|min:1|max:3',
            'extnumber' => 'required|string|min:1|max:3',
            'digital' => 'required|string|min:1|max:315',
            'dept' => 'required',
            'mail' => 'required',
            'remote' => 'required'
        ]);

        if ($this->contact_id) {
            $contacts = Contact::find($this->contact_id);
            $contacts->update([
                'namedisplay' =>$this->namedisplay,
                'lantai' => $this->lantai,
                'extnumber' =>$this->extnumber,
                'digital' => $this->digital,
                'dept' => $this->dept,
                'mail' => $this->mail,
                'remote' => $this->remote,
                'month_date' => Carbon::now(),
                'active'=>true,
            ]);

            session()->flash('message', 'Email Updated Successfully.');
            $this->resetCreateForm();
            $this->closeModalEdit();

        }
    }
    public function store()
    {
        $this->validate([
            // 'renewal_date' => 'required',
           'namedisplay' => 'required|max:25',
           //'group' => 'required',
           'dept' => 'required',
           'lantai' => 'required|string|min:1|max:3',
           'extnumber' => 'required|string|min:1|max:3',
           'digital' => 'required',
           'mail' => 'required',
           'remote' => 'required|max:25',

        ]);
       // $nextbuy=Carbon::create(order_date);
       contact::Create([
        'renewal_date' =>  Carbon::now(),
        'namedisplay' => $this->namedisplay,
        'group' => '1',
         'dept' => $this->dept,
         'lantai' => $this->lantai,
         'extnumber' => $this->extnumber,
         'digital' => $this->digital,
         'mail' => $this->mail,
         'remote' => $this->remote,
         'branch_code'=>Auth()->user()->branch_code,
         'current_team_id'=>Auth()->user()->current_team_id,
         'active'=>'1',

        ]);

        session()->flash('message', 'Data added successfully.');
        $this->resetCreateForm();
        $this->closeModal();
    }


}
