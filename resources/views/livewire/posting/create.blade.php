
<div class="fixed inset-0 z-10 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
            <div class="fixed inset-0 transition-opacity">
                <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
            <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
                role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                 <!-- button close -->
         <button wire:click="closeModal()" class="w-10 h-10 text-2xl text-white bg-green-500 -top-37 -right-7 hover:bg-red-600 focus:outline-none">
        &cross;
        </button>
        <label for="name_usr" class="text-xs font-semibold text-gray-500 uppercase md:text-sm text-light">Create Task</label>
            <form>
                @csrf
                    <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                        <div class="">
                            <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                                <div class="grid grid-cols-1"> 
                                        <label for="date_at" class="block mb-2 text-sm font-bold text-gray-700">Task Date</label>
                                        <input type="date" id="date_at" placeholder="Date at" wire:model="date_at" class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" >
                                        @error('date_at') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                <div class="grid grid-cols-1">
                                    <label for="title"  class="block mb-2 text-sm font-bold text-gray-700">Title</label>
                                    <textarea class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                                        id="title" wire:model="title"
                                        placeholder="title" require></textarea>
                                    @error('title') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                                <div class="grid grid-cols-1">
                                    <label for="desc"  class="block mb-2 text-sm font-bold text-gray-700">Detail</label>
                                    <textarea class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                                        id="desc" wire:model="desc"
                                        placeholder="Description"></textarea>
                                    @error('desc') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                              
                            </div>
                        </div>
                </div>
                <div class="px-4 py-3 bg-gray-400 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button wire:click.prevent="store()" type="button"
                            class="inline-flex justify-center w-full px-4 py-2 text-base font-bold leading-6 text-white transition duration-150 ease-in-out bg-red-600 border border-transparent rounded-md shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="flex w-full mt-3 rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModal()" type="button"
                        class="inline-flex justify-center w-full px-4 py-2 text-base font-bold leading-6 text-gray-700 transition duration-150 ease-in-out bg-white border border-gray-300 rounded-md shadow-sm hover:bg-yellow-400 hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>











                                <div class="grid grid-cols-1">
                                    <label for="date_at" class="block mb-2 text-sm font-bold text-gray-700">Task Date</label>
                                    <input type="date" id="date_at" placeholder="Date at" wire:model="date_at" class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" >
                                    @error('date_at') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>