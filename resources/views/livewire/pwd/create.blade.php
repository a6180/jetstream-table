<div class="fixed inset-0 z-10 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
            <div class="fixed inset-0 transition-opacity">
                <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
            <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
                role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                 <!-- button close -->
        <button wire:click="closeModal()" class="w-10 h-10 text-2xl text-white bg-green-500 -top-37 -right-7 hover:bg-red-600 focus:outline-none">
        &cross;
        </button>
        <label for="usage" class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Create</label>
        <form  autocomplete="off">
        @csrf
        <div class="px-4 pt-5 pb-4 bg-white:800 sm:p-6 sm:pb-4">
            <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                <div class="mb-4">
                    <label for="date_at" class="block mb-2 text-sm font-bold text-gray-700">Address</label>
                                <input class="py-2 px-3 rounded-lg border-2 border-purple-300 mt-1 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"type="text" id="address" placeholder="Enter address" wire:model="address">
                                @error('address') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
                <div class="mb-4">
                                <label for="user" class="block text-gray-700 text-sm font-bold mb-2">Username</label>
                                <input class="py-2 px-3 rounded-lg border-2 border-purple-300 mt-1 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"type="text" id="user" placeholder="Enter Username" wire:model="user">
                                @error('user') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                <div class="mb-4">
                            <label for="pass"
                                class="block text-gray-700 text-sm font-bold mb-2">Password</label>
                                <input class="py-2 px-3 rounded-lg border-2 border-purple-300 mt-1 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"type="text" id="pass" placeholder="Enter Password" wire:model="pass">
                                @error('pass') <span class="text-red-500">{{ $message }}</span>@enderror
                    </div>

                    <div class="mb-4">
                    <label for="type" class="block text-gray-700 text-sm font-bold mb-2">Type</label>
                                <input class="py-2 px-3 rounded-lg border-2 border-purple-300 mt-1 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"type="text" id="type" placeholder="Enter type" wire:model="type">
                              @error('type') <span class="text-red-500">{{ $message }}</span>@enderror
                     </div>
                </div>


                 <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                        <div class="mb-4">
                            <label for="desc" class="block mb-2 text-sm font-bold text-gray-700">Detail</label>
                               <textarea id="remark" wire:model="remark" class="py-2 px-3 w-full rounded-lg border-2 border-purple-300 mt-1 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  rows="2"  placeholder="Input Your remark"></textarea>
                               @error('remark') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                    </div>


                </div>
                <div class="bg-gray-200 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button wire:click.prevent="store()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-green-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-green-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModal()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:bg-yellow-400 hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
