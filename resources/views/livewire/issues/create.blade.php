<div class="fixed inset-0 z-10 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
            <div class="fixed inset-0 transition-opacity">
                <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
            <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
                role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                 <!-- button close -->
        <button wire:click="closeModal()" class="w-10 h-10 text-2xl text-white bg-green-500 -top-37 -right-7 hover:bg-red-600 focus:outline-none">
        &cross;
        </button>
        <label for="usage" class="text-xs font-semibold text-gray-500 uppercase md:text-sm text-light">Create Issues</label>
        <form  autocomplete="off">
        @csrf





        <div class="px-4 pt-5 pb-4 bg-white:800 sm:p-6 sm:pb-4">
            <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                <div class="mb-4">
                    <label for="module" class="block mb-2 text-sm font-bold text-gray-700">Modul/Type</label>
                        <select id="module" wire:model="module" type="" name="module" class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" >
                            <option value="">Select module</option>
                            <option value="Network">Network</option>
                            <option value="Hardware">Hardware</option>
                            <option value="Software">Software</option>
                            <option value="ERP">ERP</option>
                            <option value="Other">Other</option>
                            </select>
                        @error('module') <span class="text-red-500">{{ $message }}</span>@enderror
                    </div>


                <div class="mb-4">
                                <label for="root_cause" class="block mb-2 text-sm font-bold text-gray-700">Impact</label>
                                <input type="text" id="root_cause" placeholder="Enter Impact" wire:model="root_cause" required class="px-3 py-2 mt-1 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent">
                                @error('root_cause') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
            </div>




                    <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                        <div class="col-span-6 sm:col-span-4">
                            <label for="desc" class="block mb-2 text-sm font-bold text-gray-700">Detail</label>
                            <textarea   id="detail" wire:model="detail" class="w-full px-3 py-2 mt-1 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  rows="2"  placeholder="Input Your detail"></textarea>
                            @error('detail') <span class="text-red-500">{{ $message }}</span>@enderror
                    </div>
                            </div>


                </div>
                <div class="px-4 py-3 bg-gray-200 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button wire:click.prevent="store()" type="button"
                            class="inline-flex justify-center w-full px-4 py-2 text-base font-bold leading-6 text-white transition duration-150 ease-in-out bg-green-600 border border-transparent rounded-md shadow-sm hover:bg-green-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="flex w-full mt-3 rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModal()" type="button"
                            class="inline-flex justify-center w-full px-4 py-2 text-base font-bold leading-6 text-gray-700 transition duration-150 ease-in-out bg-white border border-gray-300 rounded-md shadow-sm hover:bg-yellow-400 hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
