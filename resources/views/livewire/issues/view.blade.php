<x-slot name="header">
    <h2 class="text-sm font-semibold leading-tight text-center text-gray-800">Issues Manager</h2>

</x-slot>

<div class="py-12">
    <div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
        <div class="px-4 py-4 overflow-hidden sm:rounded-lg">
            @if (session()->has('message'))


            <div class="p-4 mb-4 text-sm text-green-800 rounded-lg bg-green-50 dark:bg-gray-800 dark:text-green-400" role="alert">
                    <span class="font-medium">{{ session('message') }}</span>
            </div>

        @endif
            <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
            <div class="relative">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                </div>
                <input wire:model.debounce.500ms="q"  type="search" id="default-search" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-200 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search Issues, user detail ..." required>
                <button wire:click="create()"class="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">+</button>
            </div>
        </div>
            @if($isModalOpen)
            @include('livewire.issues.create')
            @elseif($isModalEdit)
            @include('livewire.issues.update')
            @endif

            <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                <table class="w-full text-sm text-left text-gray-600 dark:text-gray-600">
                    <thead class="text-xs text-gray-600 bg-gray-200 dark:bg-gray-500 ">
                        <tr>
                    <th scope="col" class="px-6 py-3">   <button wire:click="sortBy('address')">address &duarr;</button>
                            <a sortField="address" :sort-by="$sortBy" :sort-asc="$sortAsc" />
                    </th>
                    <th scope="col" class="px-6 py-3">   <button wire:click="sortBy('user')">Detail &duarr; </button>
                            <a sortField="user" :sort-by="$sortBy" :sort-asc="$sortAsc" />
                    </th>

                    <th scope="col" class="px-6 py-3">   <button wire:click="sortBy('status')">Status &duarr;</button>
                                <a sortField="status" :sort-by="$sortBy" :sort-asc="$sortAsc" />
                     </th>
                     <th scope="col" class="px-6 py-3"> Action
                    </th>

                    </tr>
                </thead>
                <tbody>





            @foreach($issues as $issue)
            <tr class="bg-white border-b dark:border-gray-200 hover:bg-gray-50 dark:hover:bg-gray-200 dark:hover:text-gray-600">

                <th scope="row" class="px-6 py-4 font-medium text-gray-600 whitespace-nowrap ">
                        <p>Modul: {{$issue->module}}</p>

                    </td>
                    <td class="px-6 py-4">
                    <p>Root Issue:  <b class="text-xs font-medium tracking-wider text-left text-red-500">{{ $issue->root_cause}}</b></p>
                        <p>Detail: <b class="text-xs font-medium tracking-wider text-left text-red-500">{{ $issue->detail}} </b></p>

                        <p>Solutions:<b class="text-xs font-medium tracking-wider text-left text-blue-500">{{ $issue->solutions}}<b>  </p>
                    </td>
                    <td class="px-6 py-4">
                        @if($issue->status)
                        <input  type="checkbox" class="inline-flex px-2 text-xs font-semibold leading-5 text-green-800 bg-green-100 rounded-full" id="ids.{{ $issue->status }}" @if($issue->status) checked @endif disabled>
                        {{ $issue->status }}
                    </td>
                         @else
                        <span class="inline-flex px-2 text-xs font-semibold leading-5 text-red-800 bg-red-100 rounded-full">{{ $issue->status }}{{ $issue->status }}</span></td>
                         @endif
                    <td class="px-6 py-4">
                        @if(auth()->user()->is_admin == 1)
                            @if($issue->active)
                            <button wire:click="markAsDisable({{ $issue->id }})" class="px-3 text-white rounded">
                                <span class="bg-gray-100 text-gray-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 hover:bg-red-400 dark:text-gray-300">Disable</span>
                            </button>

                            <button wire:click="edit({{ $issue->id }})" class="px-3 text-white rounded">
                                <span class="bg-gray-100 text-gray-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 hover:bg-green-400 dark:text-gray-300">Edit</span>
                            </button>
                            @else
                            <button wire:click="edit({{ $issue->id }})" class="px-3 text-white rounded">
                                <span class="bg-gray-100 text-gray-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 hover:bg-green-400 dark:text-gray-300">Edit</span>
                            </button>
                            @endif
                            @else
                            <button wire:click="edit({{ $issue->id }})" class="px-3 text-white rounded">
                                <span class="bg-gray-100 text-gray-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 hover:bg-green-400 dark:text-gray-300">Edit</span>
                            </button>
                        @endif
                    </td>
                    </tr>
            @endforeach

                </tbody>
            </table>
        </div>
            {{ $issues->links() }}


    </div>
</div>


