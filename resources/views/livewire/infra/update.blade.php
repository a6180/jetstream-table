<div class="fixed inset-0 z-10 overflow-y-auto ease-out duration-400">

    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">

        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>


        <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
             <!-- button close -->
                <button wire:click="updatecloseModal()"  class="w-10 h-10 text-2xl text-white bg-red-500 -top-37 -right-7 hover:bg-red-600 focus:outline-none">
                &cross;
                </button>
                <label for="name_usr" class="text-xs font-semibold text-gray-500 uppercase md:text-sm text-light">Update Computer</label>
                        <form>
                            <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                                <div class="">

                                    <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                                        <div class="grid grid-cols-1">
                                            <label for="ip_comp" class="text-xs font-semibold text-gray-500 uppercase md:text-sm text-light">IP (192.168.xx.xx)</label>
                                            <input class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"type="text" id="ip_comp" placeholder="192.168.xx.xx" wire:model="ip_comp">
                                            @error('ip_comp') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>
                                        <div class="grid grid-cols-1">
                                            <label for="hostname_comp" class="text-xs font-semibold text-gray-500 uppercase md:text-sm text-light">Host Name</label>
                                            <input class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" type="text" id="hostname_comp" wire:model="hostname_comp"
                                            placeholder="Enter HostName">
                                            @error('hostname_comp') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>
                                    </div>

                                    <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                                        <div class="grid grid-cols-1">
                                            <label for="dept_comp" class="block mb-2 text-sm font-bold text-gray-700">Departement</label>
                                                    <select id="dept_comp" wire:model="dept_comp" type="" class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  name="dept_comp">
                                                        <option value="">Departement</option>
                                                        @foreach($deptcomp as $depts)
                                                        <option value="{{ $depts->dept }}">{{ $depts->dept }} ({{ $depts->group }} )</option>
                                                        @endforeach
                                                    </select>
                                                    @error('dept_comp') <span class="text-red-500">{{ $message }}</span>@enderror
                                            </div>
                                            <div class="grid grid-cols-1">
                                            <label for="type_comp" class="text-xs font-semibold text-gray-500 uppercase md:text-sm text-light">Hardware Type</label>
                                            <select id="type_comp" wire:model="type_comp" type="" class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="type_comp">
                                                <option value="">Select Type</option>
                                                <option value="AiO">AiO</option>
                                                <option value="PC Buildup">PC Buildup</option>
                                                <option value="PC Rakitan">PC Rakitan</option>
                                                <option value="Laptop">Laptop</option>
                                                <option value="Mini PC">Mini PC</option>
                                                <option value="Other">Other</option>
                                                <option value="Server Prod">Server Prod</option>
                                                <option value="Server Test">Server Test</option>
                                                <option value="Android">Android</option>
                                            </select>
                                            @error('type_comp') <span class="text-red-500">{{ $message }}</span>@enderror
                                            </div>

                                    </div>

                                    <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">

                                            <div class="grid grid-cols-1">
                                            <label for="userpc" class="text-xs font-semibold text-gray-500 uppercase md:text-sm text-light">User</label>
                                            <input class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" type="text" id="userpc" wire:model="userpc"  placeholder="Enter User ">
                                            @error('userpc') <span class="text-red-500">{{ $message }}</span>@enderror
                                            </div>

                                            <div class="grid grid-cols-1">
                                            <label for="ups" class="text-xs font-semibold text-gray-500 uppercase md:text-sm text-light">Branding</label>
                                            <select id="brand" wire:model="brand" type="" class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="brand">
                                                <option value="">Select Branding</option>
                                                <option value="DELL">DELL</option>
                                                <option value="HP">HP</option>
                                                <option value="LENOVO">LENOVO</option>
                                                <option value="ACER">ACER</option>
                                                <option value="ASUS">ASUS</option>
                                                <option value="TP-LINK">TP-LINK</option>
                                                <option value="D-LINK">D-LINK</option>
                                                <option value="SYNOLOGY">SYNOLOGY</option>
                                                <option value="SEAGATE">SEAGATE</option>
                                                <option value="GIGABYTE">GIGABYTE</option>
                                                <option value="UNIFY">UNIFY</option>
                                                <option value="Other">Other</option>
                                            </select>
                                            @error('brand') <span class="text-red-500">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                        <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                                            <div class="grid grid-cols-1">
                                            <label for="processor_comp" class="text-xs font-semibold text-gray-500 uppercase md:text-sm text-light">Processor Type</label>
                                            <select id="processor_comp" wire:model="processor_comp" type="" class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="processor_comp">
                                                <option value="">Select Processor</option>
                                                <option value="i3">i3</option>
                                                <option value="i5">i5</option>
                                                <option value="i7">i7</option>
                                                <option value="i9">i9</option>
                                                <option value="AMD">AMD</option>
                                                <option value="Other">Other</option>
                                            </select>
                                            @error('processor_comp') <span class="text-red-500">{{ $message }}</span>@enderror
                                            </div>
                                            <div class="grid grid-cols-1">

                                            <label for="os_comp" class="text-xs font-semibold text-gray-500 uppercase md:text-sm text-light">OS Version</label>
                                            <select id="os_comp" wire:model="os_comp" type="" class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="os_comp">
                                                <option value="">Select OS Version</option>
                                                <option value="WIN xp">WIN XP</option>
                                                <option value="WIN 7">WIN 7</option>
                                                <option value="WIN 8">WIN 8</option>
                                                <option value="WIN 10">WIN 10</option>
                                                <option value="WIN 11">WIN 11</option>
                                                <option value="Kerio">Kerio</option>
                                                <option value="Linux">Linux</option>
                                                <option value="Android">Android</option>
                                                <option value="Other">Other</option>
                                            </select>
                                            @error('os_comp') <span class="text-red-500">{{ $message }}</span>@enderror


                                            </div>
                                        </div>
                                    <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                                        <div class="grid grid-cols-1">
                                        <label for="ram_comp" class="text-xs font-semibold text-gray-500 uppercase md:text-sm text-light">RAM Size</label>
                                            <input class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" type="number" id="ram_comp" placeholder="Total RAM" wire:model="ram_comp">
                                            @error('ram_comp') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>
                                        <div class="grid grid-cols-1">
                                        <label for="hdd" class="text-xs font-semibold text-gray-500 uppercase md:text-sm text-light">Hardisk Size</label>
                                            <input class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" type="number" id="hdd_comp" placeholder="Total HDD" wire:model="hdd_comp"
                                                                placeholder="Total HDD ">
                                            @error('hdd_comp') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>

                                        <div class="grid grid-cols-1">
                                            <label for="remote" class="text-xs font-semibold text-gray-500 uppercase md:text-sm text-light">Remote</label>
                                                <input class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" type="number" id="remote" placeholder="Remote ID" wire:model="remote"
                                                                    placeholder="VNC/Anydesk">
                                                @error('remote') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                                        <div class="col-span-6 sm:col-span-4">
                                            <label for="remark" value="{{ __('remark') }}" />
                                               <textarea id="remark" wire:model="remark" class="w-full px-3 py-2 mt-1 text-gray-700 border-2 border-purple-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  rows="2"  placeholder="Input Your remark"></textarea>
                                               @error('remark') <span class="text-red-500">{{ $message }}</span>@enderror
                                       </div>
                                       </div>



                                </div>

                            </div>
                            <div class="px-4 py-3 bg-gray-200 sm:px-6 sm:flex sm:flex-row-reverse">
                                <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                    <button wire:click.prevent="storeEdit()" type="button"
                                        class="inline-flex justify-center w-full px-4 py-2 text-base font-bold leading-6 text-white transition duration-150 ease-in-out bg-green-500 border border-transparent rounded-md shadow-sm hover:bg-green-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green sm:text-sm sm:leading-5">
                                        Edit
                                    </button>
                                </span>
                                <span class="flex w-full mt-3 rounded-md shadow-sm sm:mt-0 sm:w-auto">
                                    <button wire:click="updatecloseModal()" type="button"
                                        class="inline-flex justify-center w-full px-4 py-2 text-base font-bold leading-6 text-gray-700 transition duration-150 ease-in-out bg-white border border-gray-300 rounded-md shadow-sm hover:bg-yellow-400 hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue sm:text-sm sm:leading-5">
                                        Close
                                    </button>
                                </span>
                            </div>
                        </form>
        </div>
    </div>
</div>
