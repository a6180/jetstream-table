<x-slot name="header">
    <h2 class="text-center">Infrastructure</h2>
</x-slot>
<div class="py-12">
    <div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
        <div class="px-4 py-4 overflow-hidden sm:rounded-lg">
        @if (session()->has('message'))
                <!--<div class="px-4 py-3 my-3 text-gray-900 bg-green-200 border-t-4 border-gray-500 rounded-b shadow-md"
                    role="alert">
                    <div class="flex">
                        <div>
                            <p class="text-sm">{{ session('message') }}</p>
                        </div>
                    </div>
                </div>   -->

                <div class="p-4 mb-4 text-sm text-green-800 rounded-lg bg-green-50 dark:bg-gray-800 dark:text-green-400" role="alert">
                        <span class="font-medium">{{ session('message') }}</span>
                </div>

            @endif
            <di>
              <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
              <div class="relative">
                  <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                      <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                  </div>
                  <input wire:model.debounce.500ms="q"  type="search" id="default-search" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-200 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search name pc , detail ..." required>
                  <button wire:click="create()" class="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">+</button>
              </div>
              </div>
              @if($isModalOpen)
            @include('livewire.infra.create')
            @elseif($isModalUpdate)
            @include('livewire.infra.update')
            @endif
            <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                <table class="w-full text-sm text-left text-gray-600 dark:text-gray-600">
                <thead class="text-xs text-gray-600 bg-gray-200 dark:bg-gray-500 ">
                        <tr>
                    <th scope="col" class="px-6 py-3">  <button wire:click="sortBy('ip_comp')">IP &duarr;</button>
                            <a sortField="ip_comp" :sort-by="$sortBy" :sort-asc="$sortAsc" />
                    </th>
                    <th scope="col" class="px-6 py-3">  <button wire:click="sortBy('os_comp')">Detail &duarr; </button>
                            <a sortField="os_comp" :sort-by="$sortBy" :sort-asc="$sortAsc" /></th>
                    <th scope="col" class="px-6 py-3">  <button wire:click="sortBy('state')">state &duarr; </button>
                            <a sortField="state" :sort-by="$sortBy" :sort-asc="$sortAsc" /></th>
                    <th scope="col" class="px-6 py-3">  action</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($assets as $asset)
                    <tr class="bg-white border-b dark:border-gray-200 hover:bg-gray-50 dark:hover:bg-gray-200 dark:hover:text-gray-600">
                        <th scope="row" class="px-6 py-4 font-medium text-gray-600 whitespace-nowrap ">
                            <p>
                                IP:  <b class="text-xs font-medium tracking-wider text-left text-green-500">{{ $asset->ip_comp }} </b></p> <p>
                                    Remote:  <b class="text-xs font-medium tracking-wider text-left text-red-500">  {{ $asset->remote }}</b></p></th>

                                             <td class="px-6 py-4">
                                                <p>
                                                    host: <b class="text-xs font-medium tracking-wider text-left text-red-500">
                                                 {{ $asset->hostname_comp}} </b></p> os:  {{ $asset->os_comp}}  type:{{ $asset->type_comp}}  proc: {{ $asset->processor_comp}} hdd:{{ $asset->hdd_comp}} ram:{{ $asset->ram_comp}}
                       </td>
                       <td class="px-6 py-4">
                            @if($asset->active == 1)
                            <div class="form-check form-switch">
                                <input class="form-check-input"  type="checkbox" role="switch" @if($active=1) checked disabled @endif>
                            </div>
                          @else
                            <div class="form-check form-switch">
                                <input class="form-check-input"  type="checkbox" role="switch"   disabled unchecked>
                            </div>
                        @endif

                        </td>
                        <td class="px-6 py-4">
                            @if(auth()->user()->is_admin == 1)
                            @if($asset->active == 0)
                            <button wire:click="edit({{ $asset->id }})"  class="px-3 text-white">
                            <span class="bg-gray-100 text-gray-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 hover:bg-green-400 dark:text-gray-300">Edit</span>
                            </button>


                            <button wire:click="delete({{ $asset->id }})"  class="px-3 text-white">
                            <span class="bg-red-100 text-red-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-red-900 hover:bg-red-400 dark:text-red-300">Delete</span>
                            </button>




                            @else

                            <button wire:click="edit({{ $asset->id }})"  class="px-3 text-white">
                            <span class="bg-gray-100 text-gray-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 hover:bg-green-400 dark:text-gray-300">Edit</span>
                            </button>
                                @endif
                            @else



                            <button wire:click="edit({{ $asset->id }})" class="px-3 text-white">
                            <span class="bg-gray-100 text-gray-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 hover:bg-green-400 dark:text-gray-300">Edit</span>
                            </button>
                            @endif


                        </td>
                    </tr>

                    @endforeach
                </tbody>
            </table></div>
            {{ $assets->links() }}

    </div>
</div>

