<div class="fixed inset-0 z-10 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
            <div class="fixed inset-0 transition-opacity">
                <div class="absolute inset-0 bg-gray-500 opacity-75">
                    Update Email
                </div>
            </div>
     <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
                role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                 <!-- button close -->

         <button wire:click="closeModalEdit()" class="w-10 h-10 text-2xl text-white bg-green-500 -top-37 -right-7 hover:bg-red-600 focus:outline-none">
        &cross;
        </button>
            <form>
                <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                            <div class="mb-4">
                            <label for="name_usr"  class="block mb-2 text-sm font-bold text-gray-700">Name</label>
                            <input type="text"  class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                id="name_usr" placeholder="Enter name_usr" wire:model="name_usr">
                            @error('name_usr') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                        <div class="mb-4">
                            <label for="email_usr"  class="block mb-2 text-sm font-bold text-gray-700">Email</label>
                            <input type="text"  class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                id="email_usr" placeholder="Enter Email" wire:model="email_usr">
                            @error('email_usr') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                        <div class="mb-4">
                            <label for="pwd_usr"  class="block mb-2 text-sm font-bold text-gray-700">Password</label>
                            <input type="text"  class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                id="pwd_usr" placeholder="Enter pwd_usr" wire:model="pwd_usr">
                            @error('pwd_usr') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                        <div class="mb-4">   
                            <label for="email_type" class="block mb-2 text-sm font-bold text-gray-700">Type</label>
                                        <select id="email_type" wire:model="email_type" type="" name="email_type" class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline" >
                                            <option value="">Select Email Type</option>
                                            <option value="Dedicated">Dedicated</option>
                                            <option value="Hosting">Hosting</option>
                                            <option value="O365">O365</option>
                                            <option value="Gsuite">Gsuite</option> 
                                            <option value="Other">Other</option>
                                        </select>
                                        @error('email_type') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    
                    <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                    <div class="mb-4">
                        <label for="pass"
                                class="block mb-2 text-sm font-bold text-gray-700">Departement</label>
                                <select id="dept_usr" wire:model="dept_usr" type="" class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline" name="dept_usr">
                                    <option value="">Departement</option>
                                    @foreach($dept as $depts)
                                    <option value="{{ $depts->dept }}">{{ $depts->dept }} ({{ $depts->group }} )</option>
                                    @endforeach
                                </select>
                                @error('dept_usr') <span class="text-red-500">{{ $message }}</span>@enderror
                    </div>

                        <div class="mb-4">
                            <label for="remark_usr"  class="block mb-2 text-sm font-bold text-gray-700">remark</label>
                            <textarea
                            class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                id="remark_usr" wire:model="remark_usr"
                                placeholder="Description"></textarea>
                            @error('remark_usr') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                    </div>
                   </div>
                </div>

                <div class="px-4 py-3 bg-gray-100 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button wire:click.prevent="storeEdit()" type="button"
                            class="inline-flex justify-center w-full px-4 py-2 text-base font-bold leading-6 text-white transition duration-150 ease-in-out bg-red-600 border border-transparent rounded-md shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="flex w-full mt-3 rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalEdit()" type="button"
                        class="inline-flex justify-center w-full px-4 py-2 text-base font-bold leading-6 text-gray-700 transition duration-150 ease-in-out bg-white border border-gray-300 rounded-md shadow-sm hover:bg-yellow-400 hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>


            </form>
        </div>
    </div>
</div>
