<div class="fixed inset-0 z-10 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
            <div class="fixed inset-0 transition-opacity">
                <div class="absolute inset-0 bg-gray-500 opacity-75">

                </div>
            </div>
     <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
                role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                 <!-- button close -->

         <button wire:click="closeModalEdit()" class="w-10 h-10 text-2xl text-white bg-green-500 -top-37 -right-7 hover:bg-red-600 focus:outline-none">
        &cross;
        </button><b>Update Extension</b>
            <form>
                @csrf

                <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                    <div class="">

                        <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                            <div class="mb-4">
                            <label for="namedisplay"  class="block mb-2 text-sm font-bold text-gray-700">namedisplay</label>
                            <input type="text"  class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                id="namedisplay" placeholder="Enter Date" wire:model="namedisplay" required>
                            @error('namedisplay') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                        <div class="mb-4">
                            <label for="extnumber"  class="block mb-2 text-sm font-bold text-gray-700">extnumber</label>
                            <input type="number"  class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                id="extnumber" placeholder="Enter extnumber" wire:model="extnumber" disabled>
                            @error('extnumber') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                    <div class="mb-4">
                            <label for="digital" class="block mb-2 text-sm font-bold text-gray-700">digital</label>
                                        <select id="digital" wire:model="digital" type="" name="digital" class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline" >
                                            <option value="">Select Type</option>
                                            <option value="Digital">Digital</option>
                                            <option value="Analog">Analog</option>
                                        </select>
                                        @error('digital') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="dept" class="block mb-2 text-sm font-bold text-gray-700">Departement</label>
                                    <select id="dept" wire:model="dept" type="" class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline" name="dept">
                                        <option value="">Departement</option>
                                        @foreach($deptxx as $depts)
                                        <option value="{{ $depts->dept }}">{{ $depts->dept }} ({{ $depts->group }} )</option>
                                        @endforeach
                                    </select>
                                    @error('dept') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                    </div>


                    <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                        <div class="mb-4">
                            <label for="lantai"  class="block mb-2 text-sm font-bold text-gray-700">lantai</label>
                            <input type="text"  class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                id="lantai" placeholder="Enter lantai" wire:model="lantai" required>
                            @error('lantai') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                        <div class="mb-4">
                            <label for="remote"  class="block mb-2 text-sm font-bold text-gray-700">remote</label>
                            <input type="text"  class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                id="remote" placeholder="Enter remote" wire:model="remote" required>
                            @error('remote') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                    </div>


                    <div class="grid grid-cols-1 gap-5 mt-5 md:grid-cols-2 md:gap-8 mx-7">
                        <div class="mb-4">
                            <label for="mail"  class="block mb-2 text-sm font-bold text-gray-700">email</label>
                            <div class="relative mb-6">
                                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                  <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
                                </div>
                                <input type="text" id="input-group-1" id="mail" wire:model="mail"
                                placeholder="info@mail.com" required class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 " placeholder="name@mail.com">
                              </div>
                            @error('mail') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                    </div>




                     </div>
                </div>

                <div class="px-4 py-3 bg-gray-100 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button wire:click.prevent="storeEdit()" type="button"
                            class="inline-flex justify-center w-full px-4 py-2 text-base font-bold leading-6 text-white transition duration-150 ease-in-out bg-red-600 border border-transparent rounded-md shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="flex w-full mt-3 rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalEdit()" type="button"
                        class="inline-flex justify-center w-full px-4 py-2 text-base font-bold leading-6 text-gray-700 transition duration-150 ease-in-out bg-white border border-gray-300 rounded-md shadow-sm hover:bg-yellow-400 hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>


            </form>
        </div>
    </div>
</div>
