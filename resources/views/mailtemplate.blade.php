<x-slot name="header">
    <h2 class="text-sm font-semibold leading-tight text-center text-gray-800">Email Asset</h2>

</x-slot>

<div class="py-12">
    <div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
        <div class="px-4 py-4 overflow-hidden sm:rounded-lg">
    <p>Hallo <b>{{$details['nama']}}</b> berikut ini adalah komentar Anda:</p>
    <table>
      <tr>
        <td>Nama</td>
        <td>:</td>
        <td>{{$details['nama']}}</td>
      </tr>
      <tr>
        <td>Website</td>
        <td>:</td>
        <td>{{$details['website']}}</td>
      </tr>
      <tr>
        <td>Komentar</td>
        <td>:</td>
        <td>{{$details['komentar']}}</td>
      </tr>
    </table>
    <p>Terimakasih <b>{{$details['nama']}}</b> telah memberi komentar.</p>

</div>
</div>
</div>
