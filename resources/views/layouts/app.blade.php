<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>
        <link rel="shortcut icon" href="{{ asset('favicon.png') }}">
        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
        @livewireStyles
    </head>
    <body class="font-sans antialiased">
        <x-banner />

        <!-- untuk ganti warna background  -->
        <div class="min-h-screen bg-gray-400">
            @livewire('navigation-menu')

            <!-- Page Heading -->
            @if (isset($header))
            <header class="bg-gray-200 border-blue-500 shadow">
                <div class="px-4 py-1 mx-auto uppercase max-w-7xl sm:px-6 lg:px-8">
                        {{ $header }}
                    </div>
                </header>
            @endif

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>

        @stack('modals')

        @livewireScripts

        <footer class="p-4 bg-white shadow md:flex md:items-center md:justify-between md:p-6 dark:bg-gray-800">
            <span class="text-sm text-gray-500 sm:text-center dark:text-gray-400">© 2023 <a href="https://id.linkedin.com/in/harys-rifai" target="_blank" class="hover:underline">harysrifai™</a>. git.{{config('app.timezone')}}.{{ Illuminate\Foundation\Application::VERSION }} All Rights Reserved.
            </span>
            <ul class="flex flex-wrap items-center mt-3 text-sm text-gray-500 dark:text-gray-400 sm:mt-0">
                <li>
                    <a href="{{ route('posts') }}" class="mr-4 hover:underline md:mr-6 ">Actifity</a>
                </li>
                <li>
                    <a href="{{ route('assets') }}" class="mr-4 hover:underline md:mr-6">Asset</a>
                </li>
                <li>
                        <a href="{{ route('Password') }}" class="mr-4 hover:underline md:mr-6">Password Manager</a>
                </li>
                <li>
                <!-- Instagram -->
                <a href="https://id.linkedin.com/in/harys-rifai" target="_blank" class="mr-4 hover:underline md:mr-6"><img class="w-8 md:w-8 lg:w-8 filter-green" src="{{ asset('/img/ansible.png') }}" alt="App IMS" title="Linux Ansible">
                </a>
                </li>
                <li>
                </li>
                </li>
            </ul>
        </footer>
    </body>
</html>
