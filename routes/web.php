<?php

use App\Http\Livewire\Asset;
use App\Http\Livewire\Contactext;
use App\Http\Livewire\Home;
use App\Http\Livewire\HomeCharts;
use App\Http\Livewire\PostCrud;
use App\Http\Livewire\Password;
use App\Http\Livewire\Emailaccount;
use App\Http\Livewire\Issues;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('Auth.login');
});

Route::middleware([
    'auth:sanctum',config('jetstream.auth_session'),'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');


});
Route::middleware(['auth:sanctum', 'verified'])->group(function () {

  //Route::resource('assets', Asset::class);
  //Route::get('assets2', View::class)->name('assets2');
    Route::get('assets', Asset::class)->name('assets');
    Route::get('posts', PostCrud::class)->name('posts');
  //Route::get('assets', View::class)->name('assets');
    Route::get('chart', HomeCharts::class)->name('chart');
    Route::get('home', Home::class)->name('home');
    Route::get('Password', Password::class)->name('Password');
    Route::get('emails', Emailaccount::class)->name('emails');
    Route::get('issue', Issues::class)->name('issue');
    Route::get('Extension', Contactext::class)->name('Extension');

});

//Route::get('assets', Asset::class)->name('assets');
